//
//  Requests_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 17/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit
import SDWebImage



class Requests_ViewController: UIViewController {
    @IBOutlet weak var view_bottom_line_upcoming: UIView!
    @IBOutlet weak var view_bottom_line_complete: UIView!
    @IBOutlet weak var tbl_requests:UITableView!
    @IBOutlet weak var lbl_request_msg:UILabel!
    
    @IBOutlet weak var view_container:UIView!
    
    var is_complete = false
    
    @IBOutlet weak var txt_search:UITextField!
    @IBOutlet weak var btn_search:UIButton!
    
    var arr_searched_upcoming = [Model_Request]()
    var arr_searched_new_jobs = [Model_Request]()
    var arr_searched_complete = [Model_Request]()
    
    var arr_for_search_upcoming = [Model_Request]()
    var arr_for_search_new_jobs = [Model_Request]()
    var arr_for_search_complete_upcoming = [Model_Request]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txt_search.layer.cornerRadius = 3
        txt_search.clipsToBounds = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(func_reload_req), name: NSNotification.Name.init(rawValue: "reload_req"), object: nil)
        
        view_bottom_line_complete.isHidden = true
        view_bottom_line_upcoming.isHidden = false
        
        view_container.layer.cornerRadius = 2
        view_container.layer.shadowOpacity = 1.0
        view_container.layer.shadowOffset = CGSize(width: 0.0, height: 1.5)
        view_container.layer.shadowRadius = 1.0
        view_container.layer.shadowColor = UIColor .lightGray.cgColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        txt_search.isHidden = true
        btn_search.isSelected = false
        txt_search.text = ""
        
        func_get_vendor_bookings()
    }
    
    @objc func func_reload_req() {
        func_get_vendor_bookings()
        func_get_vendor_complete_bookings()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_upcoming(_ sender:UIButton) {
        is_complete = false
        
        txt_search.isHidden = true
        txt_search.text = ""
        btn_search.isSelected = false
        
        view_bottom_line_complete.isHidden = true
        view_bottom_line_upcoming.isHidden = false
        
        lbl_request_msg.text = "Not booking request yet"
        
        func_get_vendor_bookings()
    }
    
    
    @IBAction func btn_complete(_ sender:UIButton) {
        is_complete = true
        
        txt_search.isHidden = true
        txt_search.text = ""
        btn_search.isSelected = false
        
        view_bottom_line_complete.isHidden = false
        view_bottom_line_upcoming.isHidden = true
        
        lbl_request_msg.text = "Not complete any request yet"
        
        func_get_vendor_complete_bookings()
    }
    
    @IBAction func btn_Search(_ sender: UIButton) {
        btn_search.isSelected = !(btn_search.isSelected)
        
        if txt_search.isHidden {
            txt_search.isHidden = false
        } else {
            txt_search.isHidden = true
            txt_search.text = ""
            Model_Request.shared.arr_upcoming_accepted_jobs = arr_for_search_upcoming
            Model_Request.shared.arr_new_job_request = arr_for_search_new_jobs
            tbl_requests.reloadData()
        }
        
    }
    
    
    
    @IBAction func txt_Search(_ sender: UITextField) {
        if is_complete {
            arr_searched_complete = [Model_Request]()
            
            for i in 0..<arr_for_search_complete_upcoming.count {
                let model = arr_for_search_complete_upcoming[i]
                let target = model.customer_name
                if ((target as NSString?)?.range(of:txt_search.text!, options: .caseInsensitive))?.location == NSNotFound
                { } else {
                    arr_searched_complete.append(model)
                }
            }
            
            if (txt_search.text! == "") {
                arr_searched_complete = arr_for_search_complete_upcoming
            }
            Model_Request.shared.arr_completed_jobs = arr_searched_complete
            tbl_requests.reloadData()
        } else {
            arr_searched_upcoming = [Model_Request]()
            arr_searched_new_jobs = [Model_Request]()
            
            for i in 0..<arr_for_search_new_jobs.count {
                let model = arr_for_search_new_jobs[i]
                let target = model.customer_name
                if ((target as NSString?)?.range(of:txt_search.text!, options: .caseInsensitive))?.location == NSNotFound
                { } else {
                    arr_searched_new_jobs.append(model)
                }
            }

            Model_Request.shared.arr_new_job_request = arr_searched_new_jobs
            
            for i in 0..<arr_for_search_upcoming.count {
                let model = arr_for_search_upcoming[i]
                let target = model.customer_name
                if ((target as NSString?)?.range(of:txt_search.text!, options: .caseInsensitive))?.location == NSNotFound
                { } else {
                    arr_searched_upcoming.append(model)
                }
            }
            
            if (txt_search.text! == "") {
                arr_searched_upcoming = arr_for_search_upcoming
                 arr_searched_new_jobs = arr_for_search_new_jobs
            }
            
            Model_Request.shared.arr_upcoming_accepted_jobs = arr_searched_upcoming
        }
        
        tbl_requests.reloadData()
    }
    
}


extension Requests_ViewController {
    @objc func func_get_vendor_bookings() {
        func_ShowHud()
        Model_Request.shared.func_get_vendor_bookings { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status == "success" {
                    self.arr_for_search_upcoming = Model_Request.shared.arr_upcoming_accepted_jobs
                    self.arr_for_search_new_jobs = Model_Request.shared.arr_new_job_request
                    
                    if Model_Request.shared.arr_new_job_request.count == 0 && Model_Request.shared.arr_upcoming_accepted_jobs.count == 0 {
                        self.tbl_requests.isHidden = true
                        self.lbl_request_msg.isHidden = false
                    } else {
                        self.tbl_requests.isHidden = false
                        self.lbl_request_msg.isHidden = true
                    }
                } else {
                    self.tbl_requests.isHidden = true
                    self.lbl_request_msg.isHidden = false
                }
                self.tbl_requests.reloadData()
            }
        }
    }
    
    func func_get_vendor_complete_bookings() {
        func_ShowHud()
        Model_Request.shared.func_get_vendor_complete_bookings { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status == "success" {
                    self.arr_for_search_complete_upcoming=Model_Request.shared.arr_completed_jobs
                    if Model_Request.shared.arr_completed_jobs.count == 0 {
                        self.tbl_requests.isHidden = true
                        self.lbl_request_msg.isHidden = false
                    } else {
                        self.tbl_requests.isHidden = false
                        self.lbl_request_msg.isHidden = true
                    }
                } else {
                    self.tbl_requests.isHidden = true
                    self.lbl_request_msg.isHidden = false
                }
                self.tbl_requests.reloadData()
            }
        }
    }
    
}



extension Requests_ViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if is_complete {
            return 0
        } else {
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        let lbl_header = cell?.viewWithTag(1) as? UILabel
        if section == 0 {
            if Model_Request.shared.arr_new_job_request.count == 0 {
                lbl_header?.text = "No new Job Requests"
            } else {
                lbl_header?.text = "New Job Requests"
            }
        } else {
            if Model_Request.shared.arr_upcoming_accepted_jobs.count == 0 {
                lbl_header?.text = "No upcoming Accepted Jobs"
            } else {
                lbl_header?.text = "Upcoming Accepted Jobs"
            }
        }
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if is_complete {
            return 1
        } else {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if is_complete {
               return Model_Request.shared.arr_completed_jobs.count
        } else {
            if section == 0 {
                return Model_Request.shared.arr_new_job_request.count
            } else {
                return Model_Request.shared.arr_upcoming_accepted_jobs.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell-1", for: indexPath) as! Requests_TableViewCell
        
        if is_complete {
            let model = Model_Request.shared.arr_completed_jobs[indexPath.row]
            cell.lbl_name.text = model.customer_name
            cell.lbl_sub_cate.text = model.sub_cate_name
            cell.lbl_time.text = "\(model.booking_date) \(model.booking_when)"
            
            cell.img_user.sd_setShowActivityIndicatorView(true)
            cell.img_user.sd_setIndicatorStyle(.gray)
            cell.img_user.sd_setImage(with:URL (string: model.customer_profile), placeholderImage:(UIImage(named:"user-gray.png")))
            
            cell.lbl_is_accepted.text = "Completed"
            cell.lbl_is_accepted.textColor = UIColor .red
        } else {
            if indexPath.section == 0 {
                let model = Model_Request.shared.arr_new_job_request[indexPath.row]
                cell.lbl_name.text = model.customer_name
                cell.lbl_sub_cate.text = model.sub_cate_name
                cell.lbl_time.text = "\(model.booking_date) \(model.booking_when)"
                
                cell.img_user.sd_setShowActivityIndicatorView(true)
                cell.img_user.sd_setIndicatorStyle(.gray)
                cell.img_user.sd_setImage(with:URL (string: model.customer_profile), placeholderImage:(UIImage(named:"user-gray.png")))
                
                cell.lbl_is_accepted.text = "Pending"
                cell.lbl_is_accepted.textColor = UIColor .lightGray
            } else {
                let model = Model_Request.shared.arr_upcoming_accepted_jobs[indexPath.row]
                cell.lbl_name.text = model.customer_name
                cell.lbl_sub_cate.text = model.sub_cate_name
                cell.lbl_time.text = "\(model.booking_date) \(model.booking_when)"
                
                cell.img_user.sd_setShowActivityIndicatorView(true)
                cell.img_user.sd_setIndicatorStyle(.gray)
                cell.img_user.sd_setImage(with:URL (string: model.customer_profile), placeholderImage:(UIImage(named:"user-gray.png")))
                
                if model.booking_status == k_accepted {
                    cell.lbl_is_accepted.text = "Accepted"
                    cell.lbl_is_accepted.textColor = k_accepted_color
                } else if model.booking_status == k_cancel_by_vendor {
                    cell.lbl_is_accepted.text = "Cancel by vendor"
                    cell.lbl_is_accepted.textColor = k_cancel_by_vendor_color
                } else if model.booking_status == k_cancel_by_user {
                    cell.lbl_is_accepted.text = "Cancel by user"
                    cell.lbl_is_accepted.textColor = k_cancel_by_user_color
                } else if model.booking_status == k_in_progress {
                    cell.lbl_is_accepted.text = "In Progress"
                    cell.lbl_is_accepted.textColor = k_in_progress_color
                } else {
                    cell.lbl_is_accepted.text = "Status is wrong"
                    cell.lbl_is_accepted.textColor = k_complete_color
                }
            }
            
        }
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if !is_complete {
            if indexPath.section == 0 {
                let model_selected_booking = Model_Request.shared.arr_new_job_request[indexPath.row]
                str_selected_Booking_id = model_selected_booking.booking_id
            } else {
                let model_selected_booking = Model_Request.shared.arr_upcoming_accepted_jobs[indexPath.row]
                str_selected_Booking_id = model_selected_booking.booking_id
            }
            let job_details = storyboard?.instantiateViewController(withIdentifier: "JobDetails_ViewController") as! JobDetails_ViewController
            present(job_details, animated: true, completion: nil)
        }
        
    }

    
}







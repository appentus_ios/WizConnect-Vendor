//
//  SignIn_ViewController.swift
//  WECONNECT
//
//  Created by Raja Vikram singh on 22/11/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
import CountryPicker
import SVProgressHUD

//import FBSDKLoginKit
//import GoogleSignIn

let statusBarColor = UIColor (red: 164.0/255.0, green: 37.0/255.0, blue: 0.0/255.0, alpha: 1.0)
let color_AppDefault = UIColor (red: 224.0/255.0, green: 59.0/255.0, blue: 11.0/255.0, alpha: 1.0)


class SignIn_ViewController: UIViewController,CountryPickerDelegate {
    //    MARK:- IBOutlets
    @IBOutlet weak var btn_continue:UIButton!
    
    @IBOutlet weak var btn_fb:UIButton!
    @IBOutlet weak var btn_google:UIButton!
    
    @IBOutlet weak var btn_terms_conditions:UIButton!
    
    @IBOutlet weak var txt_mobile_number:ACFloatingTextfield!
    @IBOutlet weak var txt_password:ACFloatingTextfield!
    
    @IBOutlet weak var picker: CountryPicker!
    
    @IBOutlet weak var view_CountryPicker: UIView!
    
    @IBOutlet weak var btn_CountryCode: UIButton!
    
    //    MARK:- vars
    
    //    MARK:- VC's life Cycle
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btn_continue.layer.cornerRadius = btn_continue.frame.size.height/2
        btn_continue.clipsToBounds = true
                
        view_CountryPicker.isHidden = true
        func_AddCountryPicker()
        
        guard
            let statusBarView = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else {
                return
        }
        statusBarView.backgroundColor = statusBarColor
        
        func_set_design()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        is_remaining_days = true
    }
    
    
    func func_set_design() {
        let underlineAttribute : [NSAttributedStringKey: Any] = [
            NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue) : UIFont (name: "Lato-Regular", size:  12.0) ?? UIFont .systemFont(ofSize: 12),
            NSAttributedStringKey(rawValue: NSAttributedStringKey.underlineColor.rawValue) : UIColor.lightGray,
            NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue) : UIColor.lightGray,
            NSAttributedStringKey(rawValue: NSAttributedStringKey.underlineStyle.rawValue) :NSUnderlineStyle.styleSingle.rawValue]
        
        let underlineAttributedString = NSAttributedString(string: " Terms & Conditions", attributes: underlineAttribute)
        
        btn_terms_conditions.setAttributedTitle(underlineAttributedString, for: .normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    MARK:- IBActions
    @IBAction func btn_terms_conditions(_ sender: Any) {
        let terms_condtion_VC = storyboard?.instantiateViewController(withIdentifier: "FAQ_ViewController") as! FAQ_ViewController
        present(terms_condtion_VC, animated: true, completion: nil)
    }
    
    @IBAction func btn_Continue(_ sender: Any) {
        
        if !func_Validation() {
            return
        }
        
        Model_SignIn.shared.vendor_country_code = btn_CountryCode.currentTitle!
        Model_SignIn.shared.vendor_mobile = txt_mobile_number.text!
        Model_SignIn.shared.vendor_password = txt_password.text!
        
        func_ShowHud()
        Model_SignIn.shared.func_SignIn { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status == "success" {
                    self.func_get_all_plan()
                } else {
                    self.func_ShowHud_Error(with: Model_SignIn.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1.5, execute: {
                        self.func_HideHud()
                    })
                }
            }
        }
    }
    
    func func_get_all_plan() {
        func_ShowHud()
        Model_Plan_Packages.shared.func_get_all_plan { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if Model_Plan_Packages.shared.remaining_days == "0" {
                    is_remaining_days = false
                    
                    let sign_VC = self.storyboard?.instantiateViewController(withIdentifier: "Plan_Packages_ViewController")
                    self.present(sign_VC!, animated: true, completion: nil)
                } else {
                    let data_dict_Result = NSKeyedArchiver.archivedData(withRootObject: Model_SignIn.shared.dict_Result_signIn)
                    UserDefaults .standard .setValue(data_dict_Result, forKey: "login_Data")

                    let sign_VC = self.storyboard?.instantiateViewController(withIdentifier: "tabbar")
                    self.present(sign_VC!, animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func btn_SignUp(_ sender: Any) {
        let sign_VC = storyboard?.instantiateViewController(withIdentifier: "SignUp_ViewController") as! SignUp_ViewController
        present(sign_VC, animated: true, completion: nil)
    }
    
    @IBAction func btn_CancelPicker(_ sender: UIButton) {
        view_CountryPicker.isHidden = true
    }
    
    @IBAction func btn_DoneCoutrnyPicker(_ sender: UIButton) {
        view_CountryPicker.isHidden = true
    }
    
    @IBAction func btn_CountryCode(_ sender: UIButton) {
        view_CountryPicker.isHidden = false
        self.view.endEditing(true)
    }
    
    //    MARK:- Custom functions
    @IBAction func btn_google (_ sender:UIButton) {

    }
    
    
    func func_AddCountryPicker() {
        let locale = Locale.current
        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
        //        picker.displayOnlyCountriesWithCodes = ["DK", "SE", "NO", "DE"] //display only
        //        picker.exeptCountriesWithCodes = ["RU"] //exept country
        let theme = CountryViewTheme(countryCodeTextColor: .black, countryNameTextColor: .black, rowBackgroundColor: .white, showFlagsBorder: false)        //optional for UIPickerView theme changes
        picker.theme = theme //optional for UIPickerView theme changes
        picker.countryPickerDelegate = self
        picker.showPhoneNumbers = true
        picker.setCountry(code!)
    }
    
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        
        print(phoneCode)
        print(countryCode)
        
        btn_CountryCode.setTitle(phoneCode, for:.normal)
    }
    
    func func_Validation() -> Bool {
        
        if txt_mobile_number.text!.isEmpty {
            func_ShowHud_Error(with: "Enter Mobile Number")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+0.5) {
                self.func_HideHud()
            }
            return false
        } else if txt_password.text!.isEmpty {
            func_ShowHud_Error(with: "Enter password")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+0.5) {
                self.func_HideHud()
            }
            return false
        } else {
            return true
        }
    }
    
    //    MARK:- Finish
}










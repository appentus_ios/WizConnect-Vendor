//
//  Model_Reviews.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 24/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import Foundation
import Alamofire

class Model_Reviews {
    static let shared = Model_Reviews()
    
    var arr_feedbacks = [Model_Reviews]()
    
    var customer_name = ""
    var customer_profile = ""
    var date = ""
    var comment = ""
    var rating = ""
    var order_count = ""
    
    var str_message = ""
    
    func func_get_feedback(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_get_feedback
        
        let param = [
                        "type":"1",
                        "id":Model_Splash.shared.vendor_id
                   ]
        
        print(param)
        
        API_WizConnect.postAPI(url: str_FullURL, parameters: param) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                self.order_count = "\(dict_JSON["order_count"]!)"
                self.arr_feedbacks.removeAll()
                let arr_Results = dict_JSON["result"] as! [[String:Any]]
                
                for dict in arr_Results {
                    self.arr_feedbacks.append(self.func_set_data(dict: dict))
                }
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_message = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
        
    }
    
    
    private func func_set_data(dict:[String:Any]) -> Model_Reviews {
        let model = Model_Reviews()
        
        model.customer_name = dict["customer_name"] as! String
        model.customer_profile = dict["customer_profile"] as! String
        model.date = dict["date"] as! String
        model.rating = dict["rating"] as! String
        model.comment = dict["comment"] as! String
        
        return model
    }

    
}




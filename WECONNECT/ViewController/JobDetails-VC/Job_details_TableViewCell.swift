//
//  Job_details_TableViewCell.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 03/01/19.
//  Copyright © 2019 Raja Vikram singh. All rights reserved.
//

import UIKit

class Job_details_TableViewCell: UITableViewCell {
    @IBOutlet weak var view_container:UIView!
    
    @IBOutlet weak var lbl_booking_status_name:UILabel!
    @IBOutlet weak var lbl_booking_status_sub_cate:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}



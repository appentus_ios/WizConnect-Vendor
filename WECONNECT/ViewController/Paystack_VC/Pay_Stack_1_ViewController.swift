//
//  ViewController.swift
//  Paystack iOS Exampe (Simple)
//

import UIKit
import Paystack

class Pay_Stack_1_ViewController: UIViewController, PSTCKPaymentCardTextFieldDelegate {
    
    @IBOutlet weak var lbl_plan_name:UILabel!
    @IBOutlet weak var lbl_plan_amount:UILabel!
    
    @IBOutlet weak var view_click_here:UIView!
    
    var str_tran_id = ""
    
    @IBAction func btn_hide_click_here(_ sender: Any) {
        view_click_here.isHidden = true
    }
    
    @IBAction func btn_back(_ sender: Any) {
        dismissKeyboardIfAny()
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func func_purchase_plan() {
        
        if str_tran_id.isEmpty {
            func_ShowHud_Error(with: "Trans id is not found")
            DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
                self.func_HideHud()
            }
            return
        }
        
        Model_Paystack.shared.plan_code =  Model_Plan_Packages.shared.plan_code
        Model_Paystack.shared.tran_id = str_tran_id
        Model_Paystack.shared.payment_status = "1"
        
        func_ShowHud()
        Model_Paystack.shared.func_purchase_plan { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status == "success" {
                    if !is_remaining_days {
                        self.func_ShowHud_Success(with: Model_Paystack.shared.str_message)
                        DispatchQueue.main.asyncAfter(deadline: .now()+1.5, execute: {
                            self.func_HideHud()
                            
                            let data_dict_Result = NSKeyedArchiver.archivedData(withRootObject: Model_SignIn.shared.dict_Result_signIn)
                            UserDefaults .standard .setValue(data_dict_Result, forKey: "login_Data")
                            
                            let sign_VC = self.storyboard?.instantiateViewController(withIdentifier: "SignIn_ViewController") as! SignIn_ViewController
                            self.present(sign_VC, animated: true, completion: nil)
                        })
                    } else {
                        self.func_ShowHud_Success(with: Model_Paystack.shared.str_message)
                    }
                } else {
                    self.func_ShowHud_Error(with: Model_Paystack.shared.str_message)
                }
                DispatchQueue.main.asyncAfter(deadline: .now()+1.5, execute: {
                    self.func_HideHud()
                })
            }
        }
    }
    
    
    let card : PSTCKCard = PSTCKCard()
    let paystackPublicKey = "pk_live_d222f3972fe4c5e8afe3c6706dba56e349545657"
    let backendURLString = "https://wizconnect.herokuapp.com"
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad();
        
        lbl_plan_name.text = Model_Plan_Packages.shared.plan_name
        lbl_plan_amount.text = Model_Plan_Packages.shared.plan_amount
        
        // hide token label and email box
        tokenLabel.text=nil
        chargeCardButton.isEnabled = false
    }
    
    // MARK: Helpers
    func showOkayableMessage(_ title: String, message: String){
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.alert
        )
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    func dismissKeyboardIfAny(){
        // Dismiss Keyboard if any
        cardDetailsForm.resignFirstResponder()
    }
    
    // MARK: Properties
    @IBOutlet weak var cardDetailsForm: PSTCKPaymentCardTextField!
    @IBOutlet weak var chargeCardButton: UIButton!
    @IBOutlet weak var tokenLabel: UILabel!
    
    // MARK: Actions
    @IBAction func cardDetailsChanged(_ sender: PSTCKPaymentCardTextField) {
        chargeCardButton.isEnabled = sender.isValid
    }
    
    @IBAction func chargeCard(_ sender: UIButton) {
        
        dismissKeyboardIfAny()
        
        // Make sure public key has been set
        if (paystackPublicKey == "" || !paystackPublicKey.hasPrefix("pk_")) {
            showOkayableMessage("You need to set your Paystack public key.", message:"You can find your public key at https://dashboard.paystack.co/#/settings/developer .")
            // You need to set your Paystack public key.
            return
        }
        
        Paystack.setDefaultPublicKey(paystackPublicKey)
        
        if cardDetailsForm.isValid {
            
            if backendURLString != "" {
                fetchAccessCodeAndChargeCard()
                return
            }
            showOkayableMessage("Backend not configured", message:"To run this sample, please configure your backend.")
            //            chargeWithSDK(newCode:"");
            
        }
        
    }
    
    func outputOnLabel(str: String){
        DispatchQueue.main.async {
            if let former = self.tokenLabel.text {
                self.tokenLabel.text = former + "\n" + str
            } else {
                self.tokenLabel.text = str
            }
        }
    }
    
    func fetchAccessCodeAndChargeCard(){
        if let url = URL(string: backendURLString  + "/new-access-code") {
            self.makeBackendRequest(url: url, message: "fetching access code", completion: { str in
                self.outputOnLabel(str: "Fetched access code: "+str)
                self.chargeWithSDK(newCode: str as NSString)
            })
        }
    }
    
    func chargeWithSDK(newCode: NSString) {
        let arr_plan_amt = Model_Plan_Packages.shared.plan_amount.components(separatedBy: " ")
        let str_plan_amt = arr_plan_amt[0]
        
//     cardParams already fetched from our view or assembled by you
        let transactionParams = PSTCKTransactionParams.init();
        
        // building new Paystack Transaction
        transactionParams.amount = UInt(Int(str_plan_amt)! * 100)
//        transactionParams.amount =  100
        
        let custom_filters: NSMutableDictionary = [
            "recurring": true
        ];
        let items: NSMutableArray = [
            "plan-1"
        ];
        
        
        
        do {
            try transactionParams.setCustomFieldValue("iOS SDK", displayedAs: "Paid Via");
            try transactionParams.setCustomFieldValue("Paystack hats", displayedAs: "To Buy");
            try transactionParams.setMetadataValue("iOS SDK", forKey: "paid_via");
            try transactionParams.setMetadataValueDict(custom_filters, forKey: "custom_filters");
            try transactionParams.setMetadataValueArray(items, forKey: "items");
        } catch {
            print(error);
        }
        transactionParams.email = "e@ma.il";
        
        // check https://developers.paystack.co/docs/split-payments-overview for details on how these work
        // transactionParams.subaccount  = "ACCT_80d907euhish8d";
        // transactionParams.bearer  = "subaccount";
        // transactionParams.transaction_charge  = 280;
        
        // if a reference is not supplied, we will give one
        // transactionParams.reference = "ChargedFromiOSSDK@"
        
        // use library to create charge and get its reference
        PSTCKAPIClient.shared().chargeCard(self.cardDetailsForm.cardParams, forTransaction: transactionParams, on: self, didEndWithError: { (error, reference) in
            
            self.outputOnLabel(str: "Charge errored")
            // what should I do if an error occured?
            print(error)
            if error._code == PSTCKErrorCode.PSTCKExpiredAccessCodeError.rawValue{
                // access code could not be used
                // we may as well try afresh
            }
            
            if error._code == PSTCKErrorCode.PSTCKConflictError.rawValue{
                // another transaction is currently being
                // processed by the SDK... please wait
            }
        
            if let errorDict = (error._userInfo as! NSDictionary?){
                if let errorString = errorDict.value(forKeyPath: "com.paystack.lib:ErrorMessageKey") as! String? {
                    if let reference=reference {
                        self.showOkayableMessage("An error occured while completing "+reference, message: errorString)
                        self.outputOnLabel(str: reference + ": " + errorString)
                        self.verifyTransaction(reference: reference)
                    } else {
                        self.showOkayableMessage("An error occured", message: errorString)
                        self.outputOnLabel(str: errorString)
                    }
                }
            }
            
            self.chargeCardButton.isEnabled = true;
        }, didRequestValidation: { (reference) in
            self.outputOnLabel(str: "requested validation: " + reference)
        }, willPresentDialog: {
            // make sure dialog can show
            // if using a "processing" dialog, please hide it
            self.outputOnLabel(str: "will show a dialog")
        }, dismissedDialog: {
            // if using a processing dialog, please make it visible again
            self.outputOnLabel(str: "dismissed dialog")
        }) { (reference) in
            self.str_tran_id  = reference
            self.func_purchase_plan()
            
//            self.outputOnLabel(str: "succeeded: " + reference)
            self.chargeCardButton.isEnabled = true;
            self.verifyTransaction(reference: reference)
        }
        return
    }
    
    func verifyTransaction(reference: String) {
        if let url = URL(string: backendURLString  + "/verify/" + reference) {
            makeBackendRequest(url: url, message: "verifying " + reference, completion:{(str) -> Void in
                self.outputOnLabel(str: "Message from paystack on verifying " + reference + ": " + str)
            })
        }
    }
    
    func makeBackendRequest(url: URL, message: String, completion: @escaping (_ result: String) -> Void){
        let session = URLSession(configuration: URLSessionConfiguration.default)
        self.outputOnLabel(str: "Backend: " + message)
        session.dataTask(with: url, completionHandler: { data, response, error in
            let successfulResponse = (response as? HTTPURLResponse)?.statusCode == 200
            if successfulResponse && error == nil && data != nil {
                if let str = NSString(data: data!, encoding: String.Encoding.utf8.rawValue){
                    completion(str as String)
                } else {
                    self.outputOnLabel(str: "<Unable to read response> while "+message)
                    print("<Unable to read response>")
                }
            } else {
                if let e=error {
                    print(e.localizedDescription)
                    self.outputOnLabel(str: e.localizedDescription)
                } else {
                    // There was no error returned though status code was not 200
                    print("There was an error communicating with your payment backend.")
                    self.outputOnLabel(str: "There was an error communicating with your payment backend while "+message)
                }
            }
        }).resume()
    }
    
    
}

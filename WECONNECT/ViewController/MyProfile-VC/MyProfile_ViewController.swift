//
//  MyProfile_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 18/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit
import SVProgressHUD
import CountryPicker
import CoreLocation


class MyProfile_ViewController: UIViewController {
    
    @IBOutlet weak var btn_CameraIcon:UIButton!
    @IBOutlet weak var img_Profile:UIImageView!
    
    @IBOutlet weak var view_1:UIView!
    @IBOutlet weak var view_2:UIView!
    @IBOutlet weak var view_3:UIView!
    
    @IBOutlet weak var view_picker:UIView!
    @IBOutlet weak var picker_profile:UIPickerView!
    
    @IBOutlet weak var btn_CountryCode: UIButton!
    @IBOutlet weak var view_CountryPicker: UIView!
    @IBOutlet weak var picker: CountryPicker!
    
    @IBOutlet weak var txt_name:UITextField!
    @IBOutlet weak var txt_email:UITextField!
    @IBOutlet weak var txt_mobile:UITextField!
    @IBOutlet weak var txt_location:UITextField!
    @IBOutlet weak var txt_OTP:UITextField!
    @IBOutlet weak var txt_About:UITextView!
    
    @IBOutlet weak var btn_update: UIButton!
    
    @IBOutlet weak var tx_select_job_category:UITextField!
    
    var str_selected_cate_code = ""
    var str_new_OTP = ""
    var str_old_OTP = ""
    var str_old_Country_code = ""
    var str_old_mobile_number = ""
    
    @IBOutlet weak var hieght_otp_view: NSLayoutConstraint!
    @IBOutlet weak var hieght_all_view: NSLayoutConstraint!
    @IBOutlet weak var hieght_profile_view: NSLayoutConstraint!
    
    @IBOutlet weak var view_otp: UIView!
    
    @IBOutlet weak var lbl_Timer:UILabel!
    @IBOutlet weak var btn_send_OTP:UIButton!
    var timer_count = 30
    var gameTimer: Timer!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view_CountryPicker.isHidden = true
        view_picker.isHidden = true
        
        btn_update.layer.cornerRadius = btn_update.frame.size.height/2
        btn_update.clipsToBounds = true
        
        txt_OTP.layer.borderColor = UIColor .lightGray.cgColor
        txt_OTP.layer.borderWidth = 1
        
        func_AddCountryPicker()
        
        NotificationCenter.default.addObserver(self, selector: #selector(func_Send_Location), name:Notification.Name(rawValue: "send_Location_Chat"), object: nil)

        btn_CameraIcon.layer.cornerRadius = btn_CameraIcon.frame.size.height/2
        btn_CameraIcon.clipsToBounds = true
        
        img_Profile.layer.cornerRadius = img_Profile.frame.size.height/2
        img_Profile.clipsToBounds = true
        
        view_1.layer.shadowOpacity = 1.0
        view_1.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        view_1.layer.shadowRadius = 1.0
        view_1.layer.shadowColor = UIColor .lightGray.cgColor
        
        view_2.layer.shadowOpacity = 1.0
        view_2.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        view_2.layer.shadowRadius = 1.0
        view_2.layer.shadowColor = UIColor .lightGray.cgColor
        
        view_3.layer.shadowOpacity = 1.0
        view_3.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        view_3.layer.shadowRadius = 1.0
        view_3.layer.shadowColor = UIColor .lightGray.cgColor
        
        func_set_data()
        
//        hieght_otp_view.constant = 0
//        hieght_all_view.constant = 880
//        hieght_profile_view.constant = 470
//        view_otp.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func func_Send_Location() {
        txt_location.text = str_selectYourLocations
    }
    
    @IBAction func btn_get_upload_docs (_ sender:UIButton) {
        let get_upload_docs = storyboard?.instantiateViewController(withIdentifier: "Work_station_photos_ViewController") as! Work_station_photos_ViewController
        present(get_upload_docs, animated: true, completion: nil)
    }
    
    @IBAction func btn_select_category (_ sender:UIButton) {
        self.view_picker.isHidden = false
        self.picker_profile.reloadAllComponents()
    }
    
    @IBAction func btn_cancel_picker (_ sender:UIButton) {
        view_picker.isHidden = true
    }
    
    @IBAction func btn_done_picker (_ sender:UIButton) {
        view_picker.isHidden = true
        
        if Model_MyProfile.shared.category_code.isEmpty {
            SVProgressHUD.showError(withStatus: "Select a category")
            
            return
        }
        
        func_ShowHud()
        Model_MyProfile.shared.func_update_category { (staus) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                self.func_ShowHud_Success(with: Model_MyProfile.shared.str_message)
                DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                    self.func_HideHud()
                    self.func_set_data()
                })
            }
        }
        
    }
    
    @IBAction func btn_select_services_You_Provides (_ sender:UIButton) {
        if !Model_MyProfile.shared.category_code.isEmpty {
            let category_VC = storyboard?.instantiateViewController(withIdentifier: "Category_ViewController") as! Category_ViewController
            present(category_VC, animated: true, completion: nil)
        } else {
            SVProgressHUD.showError(withStatus: "Select a category")
            
            return
        }
        
    }
    
    @IBAction func btn_update_profile (_ sender:Any) {
        if !func_Validation() {
            return
        }
        
        Model_MyProfile.shared.vendor_name = txt_name.text!
        Model_MyProfile.shared.vendor_email = txt_email.text!
        Model_MyProfile.shared.vendor_country_code = btn_CountryCode.currentTitle!
        Model_MyProfile.shared.vendor_mobile = txt_mobile.text!
        Model_MyProfile.shared.vendor_about = txt_About.text
        Model_MyProfile.shared.vendor_location = txt_location.text!
        Model_MyProfile.shared.vendor_lat = "\(selected_Co_Ordinates.latitude)"
        Model_MyProfile.shared.vendor_long = "\(selected_Co_Ordinates.longitude)"
        
        func_ShowHud()
        Model_MyProfile.shared.func_Update_Profile { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status == "success" {
                    self.func_ShowHud_Success(with: "Updated successfully")
                    self.timer_count = 30
                    if (self.gameTimer != nil) {
                        self.gameTimer.invalidate()
                    }
                    
                    self.btn_send_OTP.setTitle("Send OTP", for: .normal)
                } else {
                    self.func_ShowHud_Error(with: "Try again")
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                    self.func_HideHud()
                })
            }
        }
    }
    
    @IBAction func btn_select_location (_ sender:Any) {
        let map = storyboard?.instantiateViewController(withIdentifier: "Map_ViewController") as! Map_ViewController
        present(map, animated: true, completion: nil)
    }
    
    @IBAction func btn_back (_ sender:Any) {
        NotificationCenter.default.post(name: Notification.Name("update_profile"), object: nil)

        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_camera (_ sender:Any) {
        func_ChooseImage()
    }
    
    @IBAction func btn_CountryCode(_ sender: UIButton) {
        view_CountryPicker.isHidden = false
        self.view.endEditing(true)
    }
    
    
    func func_Validation() -> Bool {
        let is_Email = func_IsValidEmail(testStr: txt_email.text!)
        
        if img_Profile.image == UIImage (named: "user-gray.png") {
            func_ShowHud_Error(with: "Select image")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+1.5) {
                self.func_HideHud()
            }
            return false
        } else if txt_name.text!.isEmpty {
            func_ShowHud_Error(with: "Enter your name")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+1.5) {
                self.func_HideHud()
            }
            return false
        } else if txt_email.text!.isEmpty {
            func_ShowHud_Error(with: "Enter Email")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+1.5) {
                self.func_HideHud()
            }
            return false
        } else if !is_Email {
            func_ShowHud_Error(with: "Enter valid Email")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+1.5) {
                self.func_HideHud()
            }
            return false
        } else if txt_mobile.text!.isEmpty {
            func_ShowHud_Error(with: "Enter Phone number")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+1.5) {
                self.func_HideHud()
            }
            return false
        } else if txt_location.text!.isEmpty {
            func_ShowHud_Error(with: "Select your location")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+1.5) {
                self.func_HideHud()
            }
            return false
        } else if txt_About.textColor == UIColor .lightGray {
            func_ShowHud_Error(with: "Fill about your self")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+1.5) {
                self.func_HideHud()
            }
            return false
        } else if btn_CountryCode.currentTitle != str_old_Country_code || txt_mobile.text != str_old_mobile_number {
            if txt_OTP.text != str_new_OTP {
                func_ShowHud_Error(with: "OTP is not matched")
                
                DispatchQueue.main .asyncAfter(deadline:.now()+1.5) {
                    self.func_HideHud()
                }
                return false
            } else {
                return true
            }
        } else {
            return true
        }

    }
    
    
    @IBAction func btn_sendOTP(_ sender:Any) {

        Model_SignUp.shared.vendor_country_code = btn_CountryCode.currentTitle!
        Model_SignUp.shared.vendor_email = txt_email.text!
        Model_SignUp.shared.vendor_mobile = txt_mobile.text!
        
        func_ShowHud()
        Model_SignUp.shared.func_SendOTP { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status != "success" {

                    self.func_ShowHud_Success(with: Model_SignUp.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                        self.func_HideHud()
                    })
                    
                } else if status == "success" {
                    self.str_new_OTP = Model_SignUp.shared.str_OTP
                    
                    self.btn_send_OTP.setTitle("Resend OTP", for: .normal)
                    self.gameTimer = Timer.scheduledTimer(timeInterval:1, target: self, selector: #selector(self.runTimedCode), userInfo: nil, repeats: true)
                    self.timer_count = 30
                    
                    self.func_ShowHud_Success(with: Model_SignUp.shared.str_OTP)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                    })
                    
                }
            }
        }
    }
    
    @objc func runTimedCode() {
        timer_count = timer_count-1
        
        lbl_Timer.text = "\(timer_count)"
        
        if timer_count == 0 {
            gameTimer.invalidate()
            btn_send_OTP.setTitle("Resend OTP", for: .normal)
        }
        
    }
    
    
}



extension MyProfile_ViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Model_MyProfile.shared.arr_Result.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let model = Model_MyProfile.shared.arr_Result[row]

        return model.category_name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let model = Model_MyProfile.shared.arr_Result[row]
        
        Model_MyProfile.shared.category_id = model.category_id
        Model_MyProfile.shared.category_code = model.category_code
        str_selected_cate_code = Model_MyProfile.shared.category_code
        Model_MyProfile.shared.category_icon = model.category_icon
        Model_MyProfile.shared.category_name = model.category_name
        self.tx_select_job_category.text = model.category_name
    }
    
}



extension MyProfile_ViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func func_ChooseImage() {
        
        let alert = UIAlertController(title: "", message: "Please select", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            DispatchQueue.main.async {
                self.func_OpenCamera()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Photos", style: .default , handler:{ (UIAlertAction)in
            DispatchQueue.main.async {
                self.func_OpenGallary()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler:{ (UIAlertAction)in
            print("User click Delete button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    func func_OpenCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.delegate=self
            
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera in simulator", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func func_OpenGallary()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate=self
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            img_Profile.image = pickedImage
            Model_MyProfile.shared.img_file = img_Profile.image!
        }
        
        dismiss(animated: true, completion: nil)
    }
}



extension MyProfile_ViewController : UITextViewDelegate , CountryPickerDelegate,UITextFieldDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "About" {
            textView.text = ""
            textView.textColor = UIColor .black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "About"
            textView.textColor = UIColor .darkGray
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
//        if txt_mobile == textField {
//            if txt_mobile.text == str_old_mobile_number {
//                if btn_CountryCode.currentTitle == str_old_Country_code {
//                    str_new_OTP = str_old_OTP
//
//                    hieght_otp_view.constant = 0
//                    hieght_all_view.constant = 880
//                    hieght_profile_view.constant = 470
//                    view_otp.isHidden = true
//                } else {
//                   str_new_OTP = "-1"
//
//                   hieght_otp_view.constant = 80
//                   hieght_all_view.constant = 960
//                   hieght_profile_view.constant = 550
//                   view_otp.isHidden = false
//                }
//            } else {
//                str_new_OTP = "-1"
//
//                hieght_otp_view.constant = 80
//                hieght_all_view.constant = 960
//                hieght_profile_view.constant = 550
//                view_otp.isHidden = false
//            }
//        }
        
    }
    
    @IBAction func btn_DoneCoutrnyPicker(_ sender: UIButton) {
        view_CountryPicker.isHidden = true
    }
    
    func func_AddCountryPicker() {
        let locale = Locale.current
        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
        //        picker.displayOnlyCountriesWithCodes = ["DK", "SE", "NO", "DE"] //display only
        //        picker.exeptCountriesWithCodes = ["RU"] //exept country
        let theme = CountryViewTheme(countryCodeTextColor: .black, countryNameTextColor: .black, rowBackgroundColor: .white, showFlagsBorder: false)        //optional for UIPickerView theme changes
        picker.theme = theme //optional for UIPickerView theme changes
        picker.countryPickerDelegate = self
        picker.showPhoneNumbers = true
        picker.setCountry(code!)
    }
    
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        btn_CountryCode.setTitle(phoneCode, for:.normal)
    }
    
    @objc func func_set_data() {
        if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            let dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
            print(dict_LoginData)
            
            txt_name.text = dict_LoginData["vendor_name"] as? String
            txt_email.text = dict_LoginData["vendor_email"] as? String
            
            btn_CountryCode.setTitle(dict_LoginData["vendor_country_code"] as? String, for: .normal)
            str_old_Country_code = btn_CountryCode.currentTitle!
            txt_OTP.text = dict_LoginData["vendor_otp"] as? String
            str_new_OTP = txt_OTP.text!
            txt_mobile.text = dict_LoginData["vendor_mobile"] as? String
            str_old_mobile_number = txt_mobile.text!
            
            txt_About.text = dict_LoginData["vendor_about"] as? String
            if txt_About.text.isEmpty {
                txt_About.text = "About"
            }
            
            txt_location.text = dict_LoginData["vendor_location"] as? String
            str_selected_cate_code = dict_LoginData["vendor_cate_code"] as! String
            Model_MyProfile.shared.category_code = str_selected_cate_code
            Model_Sub_Category.shared.sub_cate_code_selected = dict_LoginData["vendor_subcate_code"] as! String
            
            let str_vendor_lat = "\(dict_LoginData["vendor_lat"]!)"
            let str_vendor_long = "\(dict_LoginData["vendor_long"]!)"
            
            Model_MyProfile.shared.vendor_lat = str_vendor_lat
            Model_MyProfile.shared.vendor_long = str_vendor_long
            
            selected_Co_Ordinates = CLLocationCoordinate2DMake(Double(str_vendor_lat)!,Double(str_vendor_long)!)
            
            img_Profile.sd_setShowActivityIndicatorView(true)
            img_Profile.sd_setIndicatorStyle(.gray)
            img_Profile.sd_setImage(with:URL (string: dict_LoginData["vendor_profile"] as! String), placeholderImage:(UIImage(named:"user-gray.png")))
        }
        
        for model in Model_MyProfile.shared.arr_Result {
            if model.category_code == self.str_selected_cate_code {
                self.tx_select_job_category.text = model.category_name
            }
        }
        
    }
    
}




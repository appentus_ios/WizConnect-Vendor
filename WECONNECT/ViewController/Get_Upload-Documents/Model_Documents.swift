//
//  Model_Job_details.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 03/01/19.
//  Copyright © 2019 Raja Vikram singh. All rights reserved.
//



import UIKit
import Foundation
import Alamofire
import SVProgressHUD


class Model_Documents {
    static let shared = Model_Documents()
    
    var str_message = ""
    
    var vendor_works_id = ""
    var vendor_works_image = ""
    var works_vendor_id = ""
    
    var arr_all_documents = [Model_Documents]()

    func func_get_about(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_get_about
        let str_params = "vendor_id=\(Model_Splash.shared.vendor_id)"
        print(str_params)
        
        API_WizConnect.func_API_Call_POST(str_URL: str_FullURL, param: str_params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                
                self.arr_all_documents.removeAll()
                for dict_json in dict_JSON["result"] as! [[String:Any]] {
                    self.arr_all_documents.append(self.func_setdata(dict: dict_json))
                }
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_message = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
    
    private func func_setdata(dict:[String:Any]) -> Model_Documents {
        let model = Model_Documents()
        
        model.vendor_works_id = "\(dict["vendor_works_id"] ?? "")"
        model.vendor_works_image = "\(dict["vendor_works_image"] ?? "")"
        model.works_vendor_id = "\(dict["works_vendor_id"] ?? "")"
        
        return model
    }
    
    
    static func func_UploadWithImage(img_works: UIImage, completionHandler:@escaping ([String:Any])->())
    {
        if Reachability.isConnectedToNetwork() {
            
            let url = URL (string: k_BaseURL+k_add_work_photo) /* your API url */
            
            let parameters = [
                "vendor_id":Model_Splash.shared.vendor_id
            ]
            print(parameters)
            
            let headers: HTTPHeaders = [
                "Content-type": "multipart/form-data"
            ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in parameters {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                if let data = UIImageJPEGRepresentation(img_works, 0.2) {
                    multipartFormData.append(data, withName:"works", fileName: "image.jpg", mimeType: "image/jpg")
                }
                
            }, usingThreshold: UInt64.init(), to: url!, method: .post, headers: headers) { (result) in
                
                print(result)
                
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do {
                            let json =  try JSONSerialization .jsonObject(with:response.data!
                                , options: .allowFragments)
                            print(json)
                            
                            completionHandler(json as! [String:Any])
                        }
                        catch let error as NSError {
                            print("error is:-",error)
                            completionHandler(["status":"false"])
                        }
                    }
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                    completionHandler(["status":"false"])
                }
            }
            
        } else {
            SVProgressHUD.showError(withStatus: "Internet Connection not Available!")
        }
    }
    
    
}



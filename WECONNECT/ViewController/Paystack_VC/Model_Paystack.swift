//
//  Model_Job_details.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 03/01/19.
//  Copyright © 2019 Raja Vikram singh. All rights reserved.
//



import Foundation


import Alamofire



class Model_Paystack {
    static let shared = Model_Paystack()
    
    var str_message = ""
    
    var plan_code = ""
    var tran_id = ""
    var payment_status = ""
    var purchase_status = ""
    
    
    func func_purchase_plan(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_purchase_plan
        let str_params = "vendor_id=\(Model_Splash.shared.vendor_id)&plan_code=\(plan_code)&tran_id=\(tran_id)&payment_status=\(payment_status)&purchase_status\(purchase_status)"
        print(str_params)
        API_WizConnect.func_API_Call_POST(str_URL: str_FullURL, param: str_params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                self.str_message = dict_JSON["message"] as! String
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_message = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    

}



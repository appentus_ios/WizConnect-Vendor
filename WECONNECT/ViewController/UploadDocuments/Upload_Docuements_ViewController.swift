//
//  Upload_Docuements_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 17/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit
import ImagePicker
import SVProgressHUD


class Upload_Docuements_ViewController: UIViewController {
    @IBOutlet weak var view_uploadDocs:UIView!
    @IBOutlet weak var coll_workstations:UICollectionView!
    
    @IBOutlet weak var img_UploadDocs:UIImageView!
    @IBOutlet weak var img_RegistrationCertificates:UIImageView!
    @IBOutlet weak var img_TaxCard:UIImageView!
    
    @IBOutlet weak var img_uploadDocs_green:UIImageView!
    @IBOutlet weak var img_Registration_green:UIImageView!
    @IBOutlet weak var img_TaxCard_green:UIImageView!
    
    @IBOutlet weak var btn_submit:UIButton!

    var tag_btn_select_image = 0
    
    var arr_images = [UIImage (named: "document-2.png")]
    
    var configuration = Configuration()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view_uploadDocs.layer.cornerRadius = 2
        view_uploadDocs.layer.shadowOpacity = 1.0
        view_uploadDocs.layer.shadowOffset = CGSize(width: 0.0, height: 1.5)
        view_uploadDocs.layer.shadowRadius = 1.0
        view_uploadDocs.layer.shadowColor = UIColor .lightGray.cgColor
        
        img_UploadDocs.layer.cornerRadius = 8
        img_UploadDocs.clipsToBounds = true
        
        img_RegistrationCertificates.layer.cornerRadius = 8
        img_RegistrationCertificates.clipsToBounds = true
        
        btn_submit.layer.cornerRadius = btn_submit.frame.size.height/2
        btn_submit.clipsToBounds = true
        
        img_TaxCard.layer.cornerRadius = 8
        img_TaxCard.clipsToBounds = true
        
        configuration.doneButtonTitle = "Done"
        configuration.noImagesTitle = "Sorry! There are no images here!"
        configuration.recordLocation = false
        
        let imagePicker = ImagePickerController(configuration: configuration)
        imagePicker.imageLimit = 6
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_back(_ sender:UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_submit (_ sender:UIButton) {
        
//        if img_UploadDocs.image == UIImage (named: "document-2.png") {
//            SVProgressHUD.showError(withStatus: "Upload Identification documents")
//
//            return
//        } else if img_RegistrationCertificates.image == UIImage (named: "document-2.png") {
//            SVProgressHUD.showError(withStatus: "Upload Registration certificats")
//
//            return
//        } else if img_TaxCard.image == UIImage (named: "document-2.png") {
//            SVProgressHUD.showError(withStatus: "Upload Tax card")
//
//            return
//        } else
            if arr_images.contains(UIImage (named: "document-2.png")) {
                SVProgressHUD.showError(withStatus: "Upload Work Station Photos")
                
                return
            }
        
        Model_SignUp.shared.img_file = img_UploadDocs.image!
        Model_SignUp.shared.img_file_1 = img_RegistrationCertificates.image!
        Model_SignUp.shared.img_file_2 = img_TaxCard.image!
        Model_SignUp.shared.arr_img_works = arr_images as! [UIImage]
        
        func_ShowHud()
        Model_SignUp.shared.func_UploadWithImage { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status == "success" {
                    let success = self.storyboard?.instantiateViewController(withIdentifier: "Success_Registration_ViewController") as! Success_Registration_ViewController
                    self.present(success, animated:true, completion: nil)
                } else {
                    DispatchQueue.main.async {
                        self.func_ShowHud_Error(with: Model_SignUp.shared.str_message)
                        DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                            self.func_HideHud()
                        })
                    }
                }
                
            }
        }
        
    }
    
    @IBAction func btn_select_image (_ sender:UIButton) {
        tag_btn_select_image = sender.tag
        func_ChooseImage()
    }
    
    @IBAction func btn_select_image_group (_ sender:UIButton) {
        let imagePickerController = ImagePickerController()
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
}

extension Upload_Docuements_ViewController : ImagePickerDelegate {
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
     
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        arr_images = images
        coll_workstations.reloadData()
        
        dismiss(animated: true, completion: nil)
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
           dismiss(animated: true, completion: nil)
    }
    
    
}



extension Upload_Docuements_ViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func func_ChooseImage() {
        
        let alert = UIAlertController(title: "", message: "Please Select", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            DispatchQueue.main.async {
                self.func_OpenCamera()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Photos", style: .default , handler:{ (UIAlertAction)in
            DispatchQueue.main.async {
                self.func_OpenGallary()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler:{ (UIAlertAction)in
            print("User click Delete button")
        }))
        
        alert.view.tintColor = color_AppDefault
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    func func_OpenCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.delegate=self
            
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera in simulator", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func func_OpenGallary()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate=self
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            if tag_btn_select_image == 1 {
                img_UploadDocs.image = pickedImage
                img_uploadDocs_green.image = UIImage (named: "upload-green.png")
            } else if tag_btn_select_image == 2 {
                 img_RegistrationCertificates.image = pickedImage
                  img_Registration_green.image = UIImage (named: "upload-green.png")
            } else if tag_btn_select_image == 3 {
                 img_TaxCard.image = pickedImage
                  img_TaxCard_green.image = UIImage (named: "upload-green.png")
            } else {
                
            }
           
        }
        
        dismiss(animated: true, completion: nil)
    }
}


//  MARK:- UICollectionView methods
extension Upload_Docuements_ViewController:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize (width: collectionView.frame.size.width/3-10, height: 90)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr_images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let coll_Car = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! Upload_Documents_CollectionViewCell
        
        coll_Car.img_workstations.image = arr_images[indexPath.row]
      
        return coll_Car
    }
    
}





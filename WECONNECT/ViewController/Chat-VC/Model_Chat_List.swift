//
//  Model_Chat_List.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 27/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import Foundation
import Alamofire


class Model_Chat_List {
    static let shared = Model_Chat_List()
    
    var fri_id = ""
    
    var date = ""
    var time = ""
    var msg = ""
    var customer_name = ""
    var customer_profile = ""
    
    var str_msg_response = ""
    
    var arr_chat_list = [Model_Chat_List]()
    
    func func_get_chat_user_list(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL_chat+k_get_chat_user_list
        
        let str_Params = [
            "user_id":Model_Splash.shared.vendor_id,
            "receiver":""
        ]
        print(str_Params)
        
        API_WizConnect.postAPI(url: str_FullURL, parameters: str_Params) {
            (dict_JSON) in
            print(dict_JSON)
            
            self.arr_chat_list.removeAll()

            if dict_JSON["status"] as? String == "success" {
                for dict_json in dict_JSON["result"] as! [[String:Any]] {
                    self.arr_chat_list.append(self.func_set_chat_data(dict: dict_json))
                }
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
    }
    
    private func func_set_chat_data(dict:[String:Any]) -> Model_Chat_List {
        let model = Model_Chat_List()
        
        let str_fri_id = dict["fri_id"] as! String
        
        if str_fri_id == Model_Splash.shared.vendor_id {
            model.fri_id = dict["user_id"] as! String
        } else {
            model.fri_id = dict["fri_id"] as! String
        }

        model.msg = dict["msg"] as! String
        model.time = dict["time"] as! String
        
        if let dict_user_detail = dict["user_detail"] as? [String:Any] {
            if let name = dict_user_detail["customer_name"] as? String {
                model.customer_name = name
            }
            
            if let customer_profile = dict_user_detail["customer_profile"] as? String {
                model.customer_profile = customer_profile
            }
            
            if let vendor_name = dict_user_detail["vendor_name"] as? String {
                model.customer_name = vendor_name
            }
            
            if let vendor_profile = dict_user_detail["vendor_profile"] as? String {
                model.customer_profile = vendor_profile
            }
        }
        
        return model
    }
    
}





//
//  Your_Reviews_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 17/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit
import SDWebImage


class Your_Reviews_ViewController: UIViewController {
    
    @IBOutlet weak var view_container:UIView!
    @IBOutlet weak var tbl_your_reviews:UITableView!
    @IBOutlet weak var lbl_no_reviews_yet:UILabel!
    
    @IBOutlet weak var view_picker:UIView!
    @IBOutlet weak var picker:UIPickerView!
    
    @IBOutlet weak var lbl_sort_reviews:UILabel!
    @IBOutlet weak var lbl_filter_reviews:UILabel!
    
    var str_sort_reviews="All Reviews"
    var str_filter_reviews="Newest First"
    var is_sort = false
    
    let arr_sort_reviews = ["All Reviews","1","2","3","4","5"]
    let arr_filter_reviews = ["Newest First","Ascending","Descending"]
    
    var arr_picker = [String]()
    
    var is_sort_reviews = false
    var is_filter = false

    
    @IBOutlet weak var txt_search:UITextField!
    @IBOutlet weak var btn_search:UIButton!
    
    var arr_searched = [Model_Reviews]()
    var arr_for_search = [Model_Reviews]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arr_for_search=Model_Reviews.shared.arr_feedbacks
        
        view_picker.isHidden = true
        
        view_container.layer.cornerRadius = 2
        view_container.layer.shadowOpacity = 1.0
        view_container.layer.shadowOffset = CGSize(width: 0.0, height: 1.5)
        view_container.layer.shadowRadius = 1.0
        view_container.layer.shadowColor = UIColor .lightGray.cgColor
        
        if Model_Reviews.shared.arr_feedbacks.count==0 {
            tbl_your_reviews.isHidden = true
            lbl_no_reviews_yet.isHidden = false
        } else {
            tbl_your_reviews.isHidden = false
            lbl_no_reviews_yet.isHidden = true
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        txt_search.isHidden = true
        btn_search.isSelected = false
        txt_search.text = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_done_picker(_ sender:Any) {
        view_picker.isHidden = true
    }
    
    @IBAction func btn_sort_reviews(_ sender:Any) {
        is_sort_reviews = true
        arr_picker = arr_sort_reviews
        view_picker.isHidden = false
        picker.reloadAllComponents()
    }
    
    @IBAction func btn_filter_reviews(_ sender:Any) {
        is_sort_reviews = false
        arr_picker = arr_filter_reviews
        view_picker.isHidden = false
        picker.reloadAllComponents()
    }
    
    @IBAction func btn_back(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_Search(_ sender: UIButton) {
        btn_search.isSelected = !(btn_search.isSelected)
        
        if txt_search.isHidden {
            txt_search.isHidden = false
        } else {
            txt_search.isHidden = true
            txt_search.text = ""
            Model_Reviews.shared.arr_feedbacks = arr_for_search
        }
        
    }
    
    @IBAction func txt_Search(_ sender: UITextField) {
        arr_searched = [Model_Reviews]()
        
        for i in 0..<arr_for_search.count {
            let model = arr_for_search[i]
            let target = model.customer_name
            if ((target as NSString?)?.range(of:txt_search.text!, options: .caseInsensitive))?.location == NSNotFound
            { } else {
                arr_searched.append(model)
                is_filter = true
            }
        }
        
        if (txt_search.text! == "") {
            arr_searched = arr_for_search
            is_filter = false
        }
        Model_Reviews.shared.arr_feedbacks = arr_searched
        tbl_your_reviews.reloadData()
    }

}






extension Your_Reviews_ViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 120
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Model_Reviews.shared.arr_feedbacks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Your_Reviews_TableViewCell
        
        let model = Model_Reviews.shared.arr_feedbacks[indexPath.row]
        
        cell.img_customer_profile.sd_setShowActivityIndicatorView(true)
        cell.img_customer_profile.sd_setIndicatorStyle(.gray)
        cell.img_customer_profile.sd_setImage(with:URL (string: model.customer_profile), placeholderImage:(UIImage(named:"user-gray.png")))
        
        cell.customer_name.text = model.customer_name
        cell.date.text = model.date
        cell.comment.text = model.comment
        cell.rating.text = model.rating

        cell.star.rating = Float(Int(model.rating)!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}




extension Your_Reviews_ViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arr_picker.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if is_sort_reviews {
            if row == 0 {
                return arr_picker[row]
            } else {
                return "\(arr_picker[row]) star"
            }
        } else {
             return arr_picker[row]
        }
    }

    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if is_sort_reviews {
            if row == 0 {
                lbl_sort_reviews.text = "\(arr_picker[row])"
                str_sort_reviews = arr_picker[row]
            } else {
                lbl_sort_reviews.text = "\(arr_picker[row]) star"
                str_sort_reviews = arr_picker[row]
            }
        } else {
             lbl_filter_reviews.text = arr_picker[row]
             str_filter_reviews = arr_picker[row]
            
            if str_filter_reviews == "Descending" {
                 is_sort = true
            } else {
                 is_sort = false
            }
        }
       func_sort_filter()
    }
    
    func func_sort_filter() {
        var arr_sorted = [Model_Reviews]()
        if is_filter {
            arr_sorted = arr_searched
        } else {
            arr_sorted = arr_for_search
        }
        
        Model_Reviews.shared.arr_feedbacks.removeAll()
        
        for model in arr_sorted {
            if Int(str_sort_reviews) == Int(model.rating) {
                Model_Reviews.shared.arr_feedbacks.append(model)
            } else if str_sort_reviews == "All Reviews" {
                Model_Reviews.shared.arr_feedbacks.append(model)
            }
        }
        
        if str_filter_reviews == "Descending" {
            if is_sort {
                Model_Reviews.shared.arr_feedbacks=Model_Reviews.shared.arr_feedbacks.reversed()
            }
        }
        tbl_your_reviews.reloadData()
    }
    
}









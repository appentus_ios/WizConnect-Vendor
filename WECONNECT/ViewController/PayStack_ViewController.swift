//
//  PayStack_ViewController.swift
//  Uber
//
//  Created by Raja Vikram singh on 12/11/18.
//  Copyright © 2018 Rajat Pathak. All rights reserved.
//

import UIKit
import Paystack
import SVProgressHUD
import BKMoneyKit

var amt_to_add = 100

class PayStack_ViewController: UIViewController, PSTCKPaymentCardTextFieldDelegate {
    
    // MARK: Properties
    //    @IBOutlet weak var cardDetailsForm: PSTCKPaymentCardTextField!
    @IBOutlet weak var chargeCardButton: UIButton!
    @IBOutlet weak var tokenLabel: UILabel!
    
    @IBOutlet weak var view_card_number: BKCardNumberField!
    @IBOutlet weak var view_expiry_date: BKCardExpiryField!
    @IBOutlet weak var txt_CVV: UITextField!
    
    @IBOutlet weak var lbl_plan_name:UILabel!
    @IBOutlet weak var lbl_plan_amount:UILabel!
   
    // MARK: REPLACE THESE

    let paystackPublicKey = "pk_live_d222f3972fe4c5e8afe3c6706dba56e349545657"
    let backendURLString = "https://wizconnect.herokuapp.com"
    
    let card : PSTCKCard = PSTCKCard()
    
    var str_tran_id = ""
    
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad();
        
        lbl_plan_name.text = Model_Plan_Packages.shared.plan_name
        lbl_plan_amount.text = Model_Plan_Packages.shared.plan_amount
        
        view_card_number.placeholder = "Card number"
        view_card_number.showsCardLogo = true
        
        view_card_number.layer.cornerRadius = 4
        view_card_number.layer.borderColor = UIColor .lightGray.cgColor
        view_card_number.layer.borderWidth = 1
        view_card_number.clipsToBounds = true
        
        view_expiry_date.layer.cornerRadius = 4
        view_expiry_date.layer.borderColor = UIColor .lightGray.cgColor
        view_expiry_date.layer.borderWidth = 1
        view_expiry_date.clipsToBounds = true
        
        chargeCardButton.layer.cornerRadius = 4
        chargeCardButton.layer.borderColor = UIColor .black.cgColor
        chargeCardButton.layer.borderWidth = 1
        chargeCardButton.clipsToBounds = true
        
        txt_CVV.layer.cornerRadius = 4
        txt_CVV.layer.borderColor = UIColor .lightGray.cgColor
        txt_CVV.layer.borderWidth = 1
        txt_CVV.clipsToBounds = true
        
        //        hide token label and email box
        //        tokenLabel.text=nil
        //        chargeCardButton.isEnabled = false
        //        clear text from card details
        //        comment these to use the sample data set
        
//                func_get_rider_card()
        
        //        cardDetailsForm.cardNumber = "5060666666666"
    }
    
    // MARK: Helpers
    func showOkayableMessage(_ title: String, message: String) {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.alert
        )
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    func dismissKeyboardIfAny() {
        // Dismiss Keyboard if any
        //        cardDetailsForm.resignFirstResponder()
    }
    
    // MARK: Actions
    @IBAction func cardDetailsChanged(_ sender: PSTCKPaymentCardTextField) {
        chargeCardButton.isEnabled = sender.isValid
    }
    
    @IBAction func chargeCard(_ sender: UIButton) {
        dismissKeyboardIfAny()
        
        if view_card_number.cardNumber.isEmpty {
            func_ShowHud_Error(with: "Enter your card number")
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.func_HideHud()
            }
            return
        } else if view_expiry_date.text!.isEmpty {
            func_ShowHud_Error(with: "Enter your card's expiry details")
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.func_HideHud()
            }
            return
        } else if txt_CVV.text!.isEmpty {
            func_ShowHud_Error(with: "Enter card CVV")
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.func_HideHud()
            }
            return
        }
        
        SVProgressHUD.show(withStatus:"Processing...")
        
        if (paystackPublicKey == "" || !paystackPublicKey.hasPrefix("pk_")) {
            showOkayableMessage("You need to set your Paystack public key.", message:"You can find your public key at https://dashboard.paystack.co/#/settings/developer .")
            // You need to set your Paystack public key.
            return
        }
        
        Paystack.setDefaultPublicKey(paystackPublicKey)
        
        //        if cardDetailsForm.isValid {
        if backendURLString != "" {
            fetchAccessCodeAndChargeCard()
            return
        }
        showOkayableMessage("Backend not configured", message:"To run this sample, please configure your backend.")
        //            chargeWithSDK(newCode:"");
        //        }
        
    }
    
    func outputOnLabel(str: String){
        DispatchQueue.main.async {
            if let former = self.tokenLabel.text {
                self.tokenLabel.text = former + "\n" + str
            } else {
                self.tokenLabel.text = str
            }
        }
    }
    
    func fetchAccessCodeAndChargeCard() {
        if let url = URL(string: backendURLString  + "/new-access-code") {
            self.makeBackendRequest(url: url, message: "fetching access code", completion: { str in
                self.func_HideHud()
//                Model_Add_Payment_details.shared.str_tr_id = str
                
                //                self.outputOnLabel(str: "Fetched access code: "+str)
                self.chargeWithSDK(newCode: str as NSString)
            })
        }
    }
    
    func chargeWithSDK(newCode: NSString) {
        
        let str_expiry_date = view_expiry_date.text!
        let arr_expiry_date = str_expiry_date.components(separatedBy:" / ")
        print(arr_expiry_date)
        
        let transactionParams = PSTCKTransactionParams.init();
        
        let card_params = PSTCKCardParams.init()
        
        card_params.number = view_card_number.cardNumber
        card_params.expYear = UInt(arr_expiry_date[0])!
        card_params.expMonth = UInt(arr_expiry_date[1])!
        card_params.cvc = txt_CVV.text
        
        transactionParams.access_code = newCode as String;
        
        transactionParams.additionalAPIParameters = ["enforce_otp": "true"];
        transactionParams.email = "uzedoanene@gmail.com"//"ezyride.luxurytaxi@gmail.com";
        transactionParams.amount = UInt(amt_to_add);
        
        let dictParams: NSMutableDictionary = [
            "recurring": true
        ];
        let arrParams: NSMutableArray = [
            "0","go"
        ];
        do {
            try transactionParams.setMetadataValueDict(dictParams, forKey: "custom_filters");
            try transactionParams.setMetadataValueArray(arrParams, forKey: "custom_array");
        } catch {
            print(error)
        }
        
        // use library to create charge and get its reference
        PSTCKAPIClient.shared().chargeCard(card_params, forTransaction: transactionParams, on: self, didEndWithError: { (error, reference) in
            //            self.outputOnLabel(str: "Charge errored")
            // what should I do if an error occured?
            print(error)
            print(reference)
            
            if error._code == PSTCKErrorCode.PSTCKExpiredAccessCodeError.rawValue{
                // access code could not be used
                // we may as well try afresh
            }
            if error._code == PSTCKErrorCode.PSTCKConflictError.rawValue{
                // another transaction is currently being
                // processed by the SDK... please wait
            }
            if let errorDict = (error._userInfo as! NSDictionary?){
                if let errorString = errorDict.value(forKeyPath: "com.paystack.lib:ErrorMessageKey") as! String? {
                    if let reference=reference {
//                        self.showOkayableMessage("An error occured while completing "+reference, message: errorString)
                        self.outputOnLabel(str: reference + ": " + errorString)
                        self.verifyTransaction(reference: reference)
                    } else {
//                        self.showOkayableMessage("An error occured", message: errorString)
//                        self.outputOnLabel(str: errorString)
                    }
                }
            }
            self.chargeCardButton.isEnabled = true;
        }, didRequestValidation: { (reference) in
            self.outputOnLabel(str: "requested validation: " + reference)
        }, willPresentDialog: {
            // make sure dialog can show
            // if using a "processing" dialog, please hide it
            self.outputOnLabel(str: "will show a dialog")
        }, dismissedDialog: {
            // if using a processing dialog, please make it visible again
            self.outputOnLabel(str: "dismissed dialog")
            
            SVProgressHUD.show(withStatus: "Processing...")
//            DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
//                SVProgressHUD.dismiss()
//                self.dismiss(animated: true, completion: nil)
//            })
            
        }) { (reference) in
            self.outputOnLabel(str: "succeeded: " + reference)
            self.chargeCardButton.isEnabled = true;
            self.verifyTransaction(reference: reference)
        }
        return
    }
    
    func verifyTransaction(reference: String){
        if let url = URL(string: backendURLString  + "/verify/" + reference) {
            makeBackendRequest(url: url, message: "verifying " + reference, completion:{(str) -> Void in
//                self.outputOnLabel(str: "Message from paystack on verifying " + reference + ": " + str)
                DispatchQueue.main.async {
//                    self.func_Add_Account_payment()
                }
            })
        }
    }
    
    func makeBackendRequest(url: URL, message: String, completion: @escaping (_ result: String) -> Void){
        let session = URLSession(configuration: URLSessionConfiguration.default)
        self.outputOnLabel(str: "Backend: " + message)
        session.dataTask(with: url, completionHandler: { data, response, error in
            let successfulResponse = (response as? HTTPURLResponse)?.statusCode == 200
            if successfulResponse && error == nil && data != nil {
                if let str = NSString(data: data!, encoding: String.Encoding.utf8.rawValue){
                    completion(str as String)
                } else {
                    self.outputOnLabel(str: "<Unable to read response> while "+message)
                    print("<Unable to read response>")
                }
            } else {
                if let e=error {
                    print(e.localizedDescription)
                    self.outputOnLabel(str: e.localizedDescription)
                } else {
                    print("There was an error communicating with your payment backend.")
                }
            }
        }).resume()
    }
    
    @IBAction func btn_back(_ sender: Any) {
        dismissKeyboardIfAny()
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    func func_purchase_plan() {
        
        if str_tran_id.isEmpty {
            func_ShowHud_Error(with: "Trans id is not found")
            DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
                self.func_HideHud()
            }
            return
        }
        
        Model_Paystack.shared.plan_code =  Model_Plan_Packages.shared.plan_code
        Model_Paystack.shared.tran_id = str_tran_id
        Model_Paystack.shared.payment_status = "1"
        
        func_ShowHud()
        Model_Paystack.shared.func_purchase_plan { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status == "success" {
                    if !is_remaining_days {
                        self.func_ShowHud_Success(with: Model_Paystack.shared.str_message)
                        DispatchQueue.main.asyncAfter(deadline: .now()+1.5, execute: {
                            self.func_HideHud()
                            
                            let data_dict_Result = NSKeyedArchiver.archivedData(withRootObject: Model_SignIn.shared.dict_Result_signIn)
                            UserDefaults .standard .setValue(data_dict_Result, forKey: "login_Data")
                            
                            let sign_VC = self.storyboard?.instantiateViewController(withIdentifier: "tabbar") as! UITabBarController
                            self.present(sign_VC, animated: true, completion: nil)
                        })
                    } else {
                        self.func_ShowHud_Success(with: Model_Paystack.shared.str_message)
                    }
                } else {
                    self.func_ShowHud_Error(with: Model_Paystack.shared.str_message)
                }
                DispatchQueue.main.asyncAfter(deadline: .now()+1.5, execute: {
                    self.func_HideHud()
                })
            }
        }
    }

    
}







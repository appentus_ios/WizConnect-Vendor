//
//  ViewController.swift
//  WECONNECT
//
//  Created by Raja Vikram singh on 22/11/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit

class Splash_ViewController: UIViewController {
    
    //    MARK:- IBOutlets
    
    //    MARK:- vars
    
    //    MARK:- VC's life Cycle
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
                let dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
                print(dict_LoginData)
                
                Model_SignIn.shared.vendor_country_code = dict_LoginData["vendor_country_code"]! as! String
                Model_SignIn.shared.vendor_mobile = dict_LoginData["vendor_mobile"]! as! String
                Model_SignIn.shared.vendor_password = dict_LoginData["vendor_password"] as! String
                k_FireBaseFCMToken = dict_LoginData["vendor_device_token"] as! String
                
                self.func_login()
            } else {
                let signIn_VC = self.storyboard?.instantiateViewController(withIdentifier: "WalkThrough_ViewController") as! WalkThrough_ViewController
                self.present(signIn_VC, animated: true, completion: nil)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    MARK:- IBActions
    
    //    MARK:- Custom functions
    func func_login() {
        func_ShowHud()
        Model_SignIn.shared.func_SignIn { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status == "success" {
                    self.func_get_all_plan()
                }  else {
                    let signIn_VC = self.storyboard?.instantiateViewController(withIdentifier: "WalkThrough_ViewController") as! WalkThrough_ViewController
                    self.present(signIn_VC, animated: true, completion: nil)
                }
            }
        }
    }
    
    func func_get_all_plan() {
        func_ShowHud()
        Model_Plan_Packages.shared.func_get_all_plan { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if Model_Plan_Packages.shared.remaining_days == "0" {
                    is_remaining_days = false
                    
                    let sign_VC = self.storyboard?.instantiateViewController(withIdentifier: "WalkThrough_ViewController")
                    self.present(sign_VC!, animated: true, completion: nil)
                } else {
                    let data_dict_Result = NSKeyedArchiver.archivedData(withRootObject: Model_SignIn.shared.dict_Result_signIn)
                    UserDefaults .standard .setValue(data_dict_Result, forKey: "login_Data")
                    
                    let sign_VC = self.storyboard?.instantiateViewController(withIdentifier: "tabbar")
                    self.present(sign_VC!, animated: true, completion: nil)
                }
            }
        }
    }

    //    MARK:- Finish
}

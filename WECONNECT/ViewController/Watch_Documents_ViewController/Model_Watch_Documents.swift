//
//  Model_Job_details.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 03/01/19.
//  Copyright © 2019 Raja Vikram singh. All rights reserved.
//



import Foundation


import Alamofire



class Model_Watch_Documents {
    static let shared = Model_Watch_Documents()
    
    var doc_id = ""
    var str_message = ""
    
    func func_delete_work_photo(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_delete_work_photo
        let str_params = "vendor_id=\(Model_Splash.shared.vendor_id)&doc_id=\(doc_id)"
        print(str_params)
        API_WizConnect.func_API_Call_POST(str_URL: str_FullURL, param: str_params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                self.str_message = dict_JSON["message"] as! String
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_message = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
    func func_add_work_photo(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_add_work_photo
        let str_params = "vendor_id=\(Model_Splash.shared.vendor_id)&doc_id=\(doc_id)"
        print(str_params)
        API_WizConnect.func_API_Call_POST(str_URL: str_FullURL, param: str_params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                self.str_message = dict_JSON["message"] as! String
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_message = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }

    
}



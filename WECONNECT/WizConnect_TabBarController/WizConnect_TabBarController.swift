//
//  WizConnect_TabBarController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 17/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit

var tabBar_selectedIndex = 0

class WizConnect_TabBarController: UITabBarController , UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
        
        guard
            let statusBarView = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else {
                return
        }
        statusBarView.backgroundColor = statusBarColor
        
        selectedIndex = 2
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }

//   override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
//    
//    tabBar_selectedIndex = self.selectedIndex
//    
//    if item.title == "ACCOUNT" || item.title == "NOTIFICATIONS" || item.title == "CHAT" {
//        tabBar.items![2].title = "REVIEWS"
//        tabBar.items![2].image = UIImage (named: "star-tabbar")
//        tabBar.items![2].selectedImage = UIImage (named: "star-tabbar")
//        
//    } else if item.title == "REQUESTS"  || item.title == "CATEGORIES"  {
//        
//        tabBar.items![2].title = "CATEGORIES"
//        tabBar.items![2].image = UIImage (named: "category")
//        tabBar.items![2].selectedImage = UIImage (named: "category")
//        
//        if item.title == "CATEGORIES"  {
//            let category_VC = storyboard?.instantiateViewController(withIdentifier: "Category_ViewController") as! Category_ViewController
//            present(category_VC, animated: true, completion: nil)
//        }
//        
//    }
//    
//    }
    
}



//
//  Model_Request.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 03/01/19.
//  Copyright © 2019 Raja Vikram singh. All rights reserved.
//

import Foundation
import Alamofire



class Model_Request {
    static let shared = Model_Request()
    
    var arr_new_job_request = [Model_Request]()
    var arr_upcoming_accepted_jobs = [Model_Request]()
    var arr_completed_jobs = [Model_Request]()
    
    var str_message = ""
    
    var booking_id  = ""
    var booking_date  = ""
    var booking_customer_id  = ""
    var booking_vendor_id  = ""
    var booking_when  = ""
    var booking_where  = ""
    var booking_lat  = ""
    var booking_long  = ""
    var booking_address  = ""
    var booking_full_address  = ""
    var booking_landmark  = ""
    var booking_cat  = ""
    var booking_subcat  = ""
    var booking_charge  = ""
    var booking_status  = ""
    
    var payment_status  = ""
    
    var customer_id  = ""
    var customer_name  = ""
    var customer_email  = ""
    var customer_social  = ""
    var customer_country_code  = ""
    var customer_mobile  = ""
    var customer_profile  = ""
    var customer_otp  = ""
    var customer_device_type  = ""
    var customer_device_token  = ""
    var customer_password  = ""
    var customer_status  = ""
    var category_name = ""
    var sub_cate_name = ""
    
    
    
    func func_get_vendor_bookings(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_get_vendor_bookings
        let str_params = "vendor_id=\(Model_Splash.shared.vendor_id)"
        
        API_WizConnect.func_API_Call_POST(str_URL: str_FullURL, param: str_params) {
            (dict_JSON) in
            print(dict_JSON)
            
            self.arr_new_job_request.removeAll()
            self.arr_upcoming_accepted_jobs.removeAll()
            
            if dict_JSON["status"] as? String == "success" {
                let arr_Results = dict_JSON["result"] as! [[String:Any]]
                
                for dict in arr_Results {
                    let str_booking_status = dict["booking_status"] as! String
                    if str_booking_status == "0" {
                        self.arr_new_job_request.append(self.func_set_data(dict: dict))
                    } else {
                        self.arr_upcoming_accepted_jobs.append(self.func_set_data(dict: dict))
                    }
                }
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_message = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
    
    
    func func_get_vendor_complete_bookings(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_get_vendor_complete_bookings
        let str_params = "vendor_id=\(Model_Splash.shared.vendor_id)"
        
        API_WizConnect.func_API_Call_POST(str_URL: str_FullURL, param: str_params) {
            (dict_JSON) in
            print(dict_JSON)
            
            self.arr_completed_jobs.removeAll()
            
            if dict_JSON["status"] as? String == "success" {
                let arr_Results = dict_JSON["result"] as! [[String:Any]]
                
                for dict in arr_Results {
                    self.arr_completed_jobs.append(self.func_set_data(dict: dict))
                }
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_message = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }
    
    private func func_set_data(dict:[String:Any]) -> Model_Request {
        let model = Model_Request()
        
        model.booking_id  = "\(dict["booking_id"] ?? "")"
        model.booking_date  = "\(dict["booking_date"] ?? "")"
        model.booking_customer_id  = "\(dict["booking_customer_id"] ?? "")"
        model.booking_vendor_id  = "\(dict["booking_vendor_id"] ?? "")"
        model.booking_when  = "\(dict["booking_when"] ?? "")"
        model.booking_where  = "\(dict["booking_where"] ?? "")"
        model.booking_lat  = "\(dict["booking_lat"] ?? "")"
        model.booking_long  = "\(dict["booking_long"] ?? "")"
        model.booking_address  = "\(dict["booking_address"] ?? "")"
        model.booking_full_address  = "\(dict["booking_full_address"] ?? "")"
        model.booking_landmark  = "\(dict["booking_landmark"] ?? "")"
        model.booking_cat  = "\(dict["booking_cat"] ?? "")"
        model.booking_subcat  = "\(dict["booking_subcat"] ?? "")"
        model.booking_charge  = "\(dict["booking_charge"] ?? "")"
        model.booking_status  = "\(dict["booking_status"] ?? "")"
        
        model.payment_status  = "\(dict["payment_status"] ?? "")"
        
        model.customer_id  = "\(String(describing: dict["customer_id"]!))"
        model.customer_name  = "\(dict["customer_name"] ?? "")"
        model.customer_email  = "\(dict["customer_email"] ?? "")"
        model.customer_social  = "\(dict["customer_social"] ?? "")"
        model.customer_country_code  = "\(dict["customer_country_code"] ?? "")"
        model.customer_mobile  = "\(dict["customer_mobile"] ?? "")"
        model.customer_profile  = "\(dict["customer_profile"] ?? "")"
        model.customer_otp  = "\(dict["customer_otp"] ?? "")"
        model.customer_device_type  = "\(dict["customer_device_type"] ?? "")"
        model.customer_device_token  = "\(dict["customer_device_token"] ?? "")"
        model.customer_password  = "\(dict["customer_password"] ?? "")"
        model.customer_status  = "\(dict["customer_status"] ?? "")"
        model.category_name  = "\(dict["category_name"] ?? "")"
        model.sub_cate_name  = "\(dict["sub_cate_name"] ?? "")"
        
        return model
    }

}

//
//  Category_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 17/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit
import ASStarRatingView
import SDWebImage

var is_from_sign_up = false

class Category_ViewController: UIViewController {
    
    @IBOutlet weak var tbl_subCategory:UITableView!
    
    var arr_selected_category = [Bool]()
    
    @IBOutlet weak var txt_search:UITextField!
    @IBOutlet weak var btn_search:UIButton!
    
    var arr_searched = [Model_Sub_Category]()
    var arr_for_search = [Model_Sub_Category]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        txt_search.layer.cornerRadius = 3
        txt_search.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        txt_search.isHidden = true
        btn_search.isSelected = false
        txt_search.text = ""
        
        Model_Sub_Category.shared.arr_sub_category_list.removeAll()
        
        func_ShowHud()
        Model_Sub_Category.shared.func_get_category { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                for _ in Model_Sub_Category.shared.arr_sub_category_list {
                    self.arr_selected_category.append(false)
                }
                
                let arr_cate_code_selected = Model_Sub_Category.shared.sub_cate_code_selected.components(separatedBy: ",")
                
                for str_cate_code_selected in arr_cate_code_selected {
                    for i in 0..<Model_Sub_Category.shared.arr_sub_category_list.count {
                        let model = Model_Sub_Category.shared.arr_sub_category_list[i]
                        if str_cate_code_selected == model.sub_cate_code {
                            self.arr_selected_category[i] = true
                        }
                    }
                }
                self.arr_for_search = Model_Sub_Category.shared.arr_sub_category_list
                self.tbl_subCategory.reloadData()
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func btn_Search(_ sender: UIButton) {
        btn_search.isSelected = !(btn_search.isSelected)
        
        if txt_search.isHidden {
            txt_search.isHidden = false
        } else {
            txt_search.isHidden = true
            txt_search.text = ""
            Model_Sub_Category.shared.arr_sub_category_list = arr_for_search
            self.view.endEditing(true)
            tbl_subCategory.reloadData()
        }
        
    }
    
    
    
    @IBAction func txt_Search(_ sender: UITextField) {

        arr_searched = [Model_Sub_Category]()
        
        for i in 0..<arr_for_search.count {
            let model = arr_for_search[i]
            let target = model.sub_cate_name
            if ((target as NSString?)?.range(of:txt_search.text!, options: .caseInsensitive))?.location == NSNotFound
            { } else {
                arr_searched.append(model)
            }
        }
        
        if (txt_search.text! == "") {
            arr_searched = arr_for_search
        }
        Model_Sub_Category.shared.arr_sub_category_list = arr_searched
        tbl_subCategory.reloadData()
    }

    
    
    @IBAction func btn_back(_ sender:Any) {
        Model_Sub_Category.shared.sub_category_selected = ""
        Model_Sub_Category.shared.sub_category_selected_name = ""

        for i in 0..<Model_Sub_Category.shared.arr_sub_category_list.count {
            let model = Model_Sub_Category.shared.arr_sub_category_list[i]
            
            if arr_selected_category[i] == true {
                if Model_Sub_Category.shared.sub_category_selected.isEmpty {
                    Model_Sub_Category.shared.sub_category_selected = model.sub_cate_code
                    Model_Sub_Category.shared.sub_category_selected_name = model.sub_cate_name

                } else {
                    Model_Sub_Category.shared.sub_category_selected = model.sub_cate_code+","+Model_Sub_Category.shared.sub_category_selected
                    Model_Sub_Category.shared.sub_category_selected_name = model.sub_cate_name+","+Model_Sub_Category.shared.sub_category_selected_name
                }
            }
        }
        
        NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "subCate"), object: nil)

        dismiss(animated: true, completion: nil)
    }
    
}



extension Category_ViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 200
        } else {
            return 60
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Model_Sub_Category.shared.arr_sub_category_list.count+2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-1", for: indexPath)
            
            let img_subCategoryIcon = cell.viewWithTag(1) as? UIImageView
            
            img_subCategoryIcon!.sd_setShowActivityIndicatorView(true)
            img_subCategoryIcon!.sd_setIndicatorStyle(.gray)
            img_subCategoryIcon!.sd_setImage(with:URL (string:Model_MyProfile.shared.category_icon), placeholderImage:(UIImage(named:"pipe.png")))
            
            let lbl_subCategory_name = cell.viewWithTag(2) as? UILabel
            lbl_subCategory_name?.text = Model_MyProfile.shared.category_name
            
            return cell
        } else if indexPath.row == Model_Sub_Category.shared.arr_sub_category_list.count+1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-3", for: indexPath) as! Update_Category_TableViewCell
            
            if is_from_sign_up {
                cell.btn_Update.isHidden = true
            } else {
                cell.btn_Update.isHidden = false
            }
            
            cell.btn_Update.layer.cornerRadius = cell.btn_Update.frame.size.height/2
            cell.btn_Update.clipsToBounds = true
            
            cell.btn_Update.tag = indexPath.row
            cell.btn_Update.addTarget(self, action: #selector(btn_update(_:)), for: .touchUpInside)
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-2", for: indexPath) as! Category_TableViewCell
            
            if Model_Sub_Category.shared.arr_sub_category_list.count > 0 {
                let model = Model_Sub_Category.shared.arr_sub_category_list[indexPath.row-1]
                cell.lbl_sub_category_name.text = model.sub_cate_name
            }
            
            if arr_selected_category[indexPath.row-1] {
                cell.btn_select.isSelected = true
            } else {
                cell.btn_select.isSelected = false
            }
            
            cell.btn_select.tag = indexPath.row-1
            cell.btn_select.addTarget(self, action: #selector(btn_Select(_:)), for: .touchUpInside)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    @IBAction func btn_update(_ sender:UIButton) {
        Model_Sub_Category.shared.sub_category_selected = ""
        
        if sender.tag == Model_Sub_Category.shared.arr_sub_category_list.count+1 {
            
            for i in 0..<Model_Sub_Category.shared.arr_sub_category_list.count {
                let model = Model_Sub_Category.shared.arr_sub_category_list[i]
                
                if arr_selected_category[i] == true {
                    if Model_Sub_Category.shared.sub_category_selected.isEmpty {
                        Model_Sub_Category.shared.sub_category_selected = model.sub_cate_code
                    } else {
                        Model_Sub_Category.shared.sub_category_selected = model.sub_cate_code+","+Model_Sub_Category.shared.sub_category_selected
                    }
                }
            }
            
            if Model_Sub_Category.shared.sub_category_selected.isEmpty {
                func_ShowHud_Error(with: "Select subcategory")
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                    self.func_HideHud()
                }
                return
            }
            
            func_ShowHud()
            Model_Sub_Category.shared.func_update_sub_category { (staus) in
                DispatchQueue.main.async {
                    self.func_HideHud()
                    
                    self.func_ShowHud_Success(with: Model_Sub_Category.shared.str_msg)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                        
                        if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
                            var dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
                            
                            Model_Sub_Category.shared.sub_cate_code_selected = dict_LoginData["vendor_subcate_code"] as! String
                            Model_MyProfile.shared.vendor_lat = "\(dict_LoginData["vendor_lat"]!)"
                            Model_MyProfile.shared.vendor_long = "\(dict_LoginData["vendor_long"]!)"
                            
                            dict_LoginData["vendor_subcate_code"] = Model_Sub_Category.shared.sub_category_selected
                            
                            let data_dict_Result = NSKeyedArchiver.archivedData(withRootObject: dict_LoginData)
                            UserDefaults .standard .setValue(data_dict_Result, forKey: "login_Data")
                        }
                    })
                }
            }
        }
        
    }
    
    @IBAction func btn_Select(_ sender:UIButton) {
        
        if arr_selected_category[sender.tag] {
            arr_selected_category[sender.tag] = false
        } else {
            arr_selected_category[sender.tag] = true
        }
        
        tbl_subCategory.reloadData()
    }
    
}


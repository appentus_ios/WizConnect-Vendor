//
//  WalkThroughCollectionViewCell.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 15/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit

class WalkThroughCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img_work:UIImageView!
    @IBOutlet weak var lbl_Title:UILabel!
    @IBOutlet weak var lbl_description:UILabel!
    @IBOutlet weak var curve_view:UIView!
    
    override func awakeFromNib() {
        
        let shapeView = UIView(frame: CGRect(x: 0, y: 0, width: curve_view.frame.size.width, height: curve_view.frame.size.height))
        let shapeSize = shapeView.frame.size
        shapeView.backgroundColor = UIColor.clear
        let path = CGMutablePath()
        path.move(to: CGPoint.zero)
        
        let curveWidthOneFourth = shapeSize.width / 4
        let curveHeight = shapeSize.height * 0.7
        path.addCurve(to: CGPoint(x: curveWidthOneFourth * 2, y: curveHeight), control1: .zero, control2: CGPoint(x: curveWidthOneFourth, y: curveHeight))
        path.addCurve(to: CGPoint(x: shapeSize.width, y: 0), control1: CGPoint(x: curveWidthOneFourth * 3, y: curveHeight), control2: CGPoint(x: shapeSize.width, y: 0))
        path.addLine(to: CGPoint(x: shapeSize.width, y: shapeSize.height))
        path.addLine(to: CGPoint(x: 0, y: shapeSize.height))
        path.addLine(to: CGPoint.zero)
        
        let layer = CAShapeLayer()
        layer.path = path
        layer.fillColor = UIColor.white.cgColor
        curve_view.layer.addSublayer(layer)
    }
    
    func pathCurvedForView(givenView: UIImageView, curvedPercent:CGFloat) ->UIBezierPath
    {
        let arrowPath = UIBezierPath()
        
        arrowPath.move(to: CGPoint(x:0, y:0))
        arrowPath.addLine(to: CGPoint(x:givenView.bounds.size.width, y:0))
        arrowPath.addLine(to: CGPoint(x:givenView.bounds.size.width, y:givenView.bounds.size.height - (givenView.bounds.size.height*curvedPercent)))
        arrowPath.addQuadCurve(to: CGPoint(x:-160, y:givenView.bounds.size.height - (givenView.bounds.size.height*curvedPercent)), controlPoint: CGPoint(x:givenView.bounds.size.width/2, y:givenView.bounds.size.height))
        arrowPath.addLine(to: CGPoint(x:0, y:0))
        arrowPath.close()
        
        
        return arrowPath
    }
    
    
    
    func applyCurvedPath(givenView: UIImageView,curvedPercent:CGFloat) {
        guard curvedPercent <= 1 && curvedPercent >= 0 else{
            return
        }
        
        let shapeLayer = CAShapeLayer(layer: givenView.layer)
        shapeLayer.path = self.pathCurvedForView(givenView: givenView,curvedPercent: curvedPercent).cgPath
        shapeLayer.frame = givenView.bounds
        shapeLayer.masksToBounds = true
        givenView.layer.mask = shapeLayer
    }
    
}

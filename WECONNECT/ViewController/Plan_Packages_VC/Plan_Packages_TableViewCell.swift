//
//  Plan_Packages_TableViewCell.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 18/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit

class Plan_Packages_TableViewCell: UITableViewCell {
    @IBOutlet weak var view_container:UIView!
    
    @IBOutlet weak var lbl_plan_name:UILabel!
    @IBOutlet weak var lbl_plan_limit:UILabel!
    @IBOutlet weak var lbl_plan_amount:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        view_container.layer.cornerRadius = 2
        view_container.layer.shadowOpacity = 1.0
        view_container.layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
        view_container.layer.shadowRadius = 3.0
        view_container.layer.shadowColor = UIColor .lightGray.cgColor
    }
    
    
}

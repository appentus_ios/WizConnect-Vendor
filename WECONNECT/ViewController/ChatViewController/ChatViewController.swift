


import UIKit
import Alamofire


var dict_userdetails = [String:Any]()
var dict_ride_details_1 = [String:Any]()

var str_fri_ID = ""

class ChatViewController: UIViewController {
    
    @IBOutlet weak var txt_msg:UITextView!
    @IBOutlet weak var tbl_chat:UITableView!
    @IBOutlet weak var navbar:UINavigationItem!
    
    @IBOutlet weak var btn_send:UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        txt_msg.layer.cornerRadius = 2
        txt_msg.layer.borderColor = color_AppDefault .cgColor
        txt_msg.layer.borderWidth = 1
        txt_msg.clipsToBounds = true
        
        navbar.title = Model_Chat.shared.fri_name
        func_chat_msg()
        NotificationCenter.default.addObserver(self, selector: #selector(func_chat_msg), name:Notification.Name(rawValue: "chat_incoming"), object: nil)
        
        txt_msg.layer.cornerRadius = 6
        txt_msg.clipsToBounds = true
        
        btn_send.layer.cornerRadius = btn_send.frame.size.height/2
        btn_send.clipsToBounds = true
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Model_Chat.shared.arr_chat_list.removeAll()
    }
    
    @IBAction func btn_back(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func func_chat_msg() {
        func_ShowHud()
        Model_Chat.shared.func_chat_msg { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                self.tbl_chat.reloadData()

                if Model_Chat.shared.arr_chat_list.count > 1 {
                    let indexPath = IndexPath (row:Model_Chat.shared.arr_chat_list.count-1, section: 0)
                    self.tbl_chat .scrollToRow(at:indexPath , at: UITableViewScrollPosition.bottom, animated: true)
                }
                
            }
        }
    }
    
    
    
    @IBAction func btn_send_msg (_ sender:UIButton) {
        self.view.endEditing(true)
        
        if txt_msg.text == "" || txt_msg.text == "Text your message..." {
            func_ShowHud_Error(with: "text a message")
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.func_HideHud()
            }
            return
        }
        
        func_ShowHud()
        Model_Chat.shared.msg_send = txt_msg.text!
        Model_Chat.shared.func_insert_chat { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                if status == "success" {
                    self.txt_msg.text = "Text your message..."
                    self.txt_msg.textColor = UIColor .darkGray
                    self.func_chat_msg()
                } else {
                    self.func_ShowHud_Error(with: Model_Chat.shared.str_msg_response)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                        self.func_HideHud()
                    }
                }
            }
        }
        
    }
    
}

extension ChatViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = Model_Chat.shared.arr_chat_list[indexPath.row]
        
        let messageText = model.msg
        if model.user_id != Model_Splash.shared.vendor_id {
            let width = tbl_chat.frame.size.width-80
            if func_size(width: Int(width), messageText: messageText).height < 60 {
                return 60
            }

            return func_size(width: Int(width), messageText: messageText).height+20
        } else {
            let width = tbl_chat.frame.size.width-30
            if func_size(width: Int(width), messageText: messageText).height < 60 {
                return 60
            }

            return func_size(width: Int(width), messageText: messageText).height+20
        }
        
    }
    
    func func_size (width:Int,messageText:String) -> CGSize {
        
        let size = CGSize(width:width, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimatedFrame = NSString(string: messageText).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font:UIFont (name: "Lato-Regular", size: 18.0) ?? UIFont .systemFont(ofSize: 18.0)], context: nil)
        
        return CGSize(width:estimatedFrame.width, height: estimatedFrame.height+20)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Model_Chat.shared.arr_chat_list.count //arr_msg.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = Model_Chat.shared.arr_chat_list[indexPath.row]
        
        if model.user_id != Model_Splash.shared.vendor_id {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-Incoming-msg", for: indexPath) as! Chat_Incoming_MSG_TableViewCell
            
            let str_msg = model.msg
            cell.lbl_msg?.text = str_msg
            
            cell.img_fri.sd_setShowActivityIndicatorView(true)
            cell.img_fri.sd_setIndicatorStyle(.gray)
            cell.img_fri.sd_setImage(with:URL (string: Model_Chat.shared.fri_img), placeholderImage:(UIImage(named:"user-gray.png")))
            
            let width = cell.frame.size.width-80
            cell.width_layout.constant = func_size(width: Int(width), messageText: str_msg).width
            cell.width_layout_view_bubble.constant = func_size(width: Int(width), messageText: str_msg).width+10
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell_my_outGoing_msg", for: indexPath) as! Chat_My_OutGoing_MSG_TableViewCell
            
            let str_msg = model.msg
            cell.lbl_msg?.text = str_msg
                    
            let width = cell.frame.size.width-30
            cell.width_layout.constant = func_size(width: Int(width), messageText: str_msg).width
            cell.width_layout_view_bubble.constant = func_size(width: Int(width), messageText: str_msg).width+10

            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
}




extension ChatViewController :UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Text your message..." {
            textView.textColor = UIColor .black
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.textColor = UIColor .darkGray
            textView.text = "Text your message..."
        }
    }
    
}


extension CALayer {
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer()
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x: 0, y: self.frame.height - thickness, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: self.frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x: self.frame.width - thickness, y: 0, width: thickness, height: self.frame.height)
            break
        default:
            break
        }
        
        border.backgroundColor = color.cgColor;
        
        self.addSublayer(border)
    }
    
}





class PaddingLabel: UILabel {
    
    var topInset: CGFloat
    var bottomInset: CGFloat
    var leftInset: CGFloat
    var rightInset: CGFloat
    
    required init(withInsets top: CGFloat, _ bottom: CGFloat,_ left: CGFloat,_ right: CGFloat) {
        self.topInset = top
        self.bottomInset = bottom
        self.leftInset = left
        self.rightInset = right
        super.init(frame: CGRect.zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            return contentSize
        }
    }

}

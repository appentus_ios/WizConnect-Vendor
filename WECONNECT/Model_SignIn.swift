//
//  Model_SignIn.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 20/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import Foundation
import Alamofire

class Model_SignIn {
    static let shared = Model_SignIn()
    
    var vendor_country_code = ""
    var vendor_mobile = ""
    var vendor_password = ""
    var vendor_device_type = ""
    var vendor_device_token = ""
    
    var str_OTP = ""
    
    var str_message = ""
    
    var dict_Result_signIn = [String:Any]()
    
    func func_SignIn(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_customer_login
        
        let param = ["vendor_country_code":"\(vendor_country_code)",
                    "vendor_mobile":"\(vendor_mobile)",
                    "vendor_password":"\(vendor_password)",
                    "vendor_device_type":"2",
                    "vendor_device_token":"\(k_FireBaseFCMToken)"]
        print(param)
        
        API_WizConnect.postAPI(url: str_FullURL, parameters: param) {
        (dict_JSON) in
        print(dict_JSON)
            
        if dict_JSON["status"] as? String == "success" {
            
            let arr_Result = dict_JSON["result"] as? [[String:Any]]
            self.dict_Result_signIn = arr_Result![0]
            
            Model_Splash.shared.vendor_id = "\(self.dict_Result_signIn["vendor_id"]!)"
            Model_Splash.shared.works_vendor_id = "\(self.dict_Result_signIn["vendor_works_id"]!)"
            Model_Splash.shared.vendor_works_id = "\(self.dict_Result_signIn["works_vendor_id"]!)"
            
            completionHandler(dict_JSON["status"] as! String)
        } else {
            if let str_status = dict_JSON["status"] as? String {
                if str_status == "failed" {
                    self.str_message = dict_JSON["message"] as! String
                    completionHandler(dict_JSON["status"] as! String)
                } else {
                    completionHandler("false")
                }
            } else {
                completionHandler("false")
            }
        }
    }
}

    
}




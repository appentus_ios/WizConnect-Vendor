//
//  Bank_Details_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 07/01/19.
//  Copyright © 2019 Raja Vikram singh. All rights reserved.
//

import UIKit

var is_remaining_days = true

class Bank_Details_ViewController: UIViewController {
    @IBOutlet weak var view_1:UIView!
    @IBOutlet weak var view_2:UIView!
    
    @IBOutlet weak var img_1:UIImageView!
    @IBOutlet weak var img_2:UIImageView!
    
    @IBOutlet weak var btn_purchase_plan:UIButton!
    @IBOutlet weak var view_click_here:UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        func_shadow(view: view_1)
        func_shadow(view: view_2)
        
        btn_purchase_plan.layer.cornerRadius = btn_purchase_plan.frame.size.height/2
        btn_purchase_plan.clipsToBounds = true
        
        img_1.layer.cornerRadius = img_1.frame.size.height/2
        img_1.clipsToBounds = true
        
        img_2.layer.cornerRadius = img_2.frame.size.height/2
        img_2.clipsToBounds = true
            
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func func_shadow(view:AnyObject) {
        view.layer.cornerRadius = 3
        view.layer.shadowOpacity = 3.0
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowRadius = 3.0
        view.layer.shadowColor = UIColor .lightGray.cgColor
    }
    
    @IBAction func btn_back (_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }

    
    
    @IBAction func btn_purchase_plan(_ sender:UIButton) {
        func_purchase_plan()
    }
    
    func func_purchase_plan() {
        Model_Paystack.shared.plan_code =  Model_Plan_Packages.shared.plan_code
        Model_Paystack.shared.tran_id = ""
        Model_Paystack.shared.payment_status = "0"
        
        func_ShowHud()
        Model_Paystack.shared.func_purchase_plan { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status == "success" {
                    if !is_remaining_days {
                        self.func_ShowHud_Success(with: Model_Paystack.shared.str_message)
                        DispatchQueue.main.asyncAfter(deadline: .now()+1.5, execute: {
                            self.func_HideHud()
                            
                            let data_dict_Result = NSKeyedArchiver.archivedData(withRootObject: Model_SignIn.shared.dict_Result_signIn)
                            UserDefaults .standard .setValue(data_dict_Result, forKey: "login_Data")
                            
                            let sign_VC = self.storyboard?.instantiateViewController(withIdentifier: "SignIn_ViewController") as! SignIn_ViewController
                            self.present(sign_VC, animated: true, completion: nil)
                        })
                    } else {
                         self.func_ShowHud_Success(with: Model_Paystack.shared.str_message)
                    }
                } else {
                    self.func_ShowHud_Error(with: Model_Paystack.shared.str_message)
                }
                DispatchQueue.main.asyncAfter(deadline: .now()+1.5, execute: {
                    self.func_HideHud()
                })
            }
        }
    }

    
    @IBAction func btn_hide_click_here(_ sender: Any) {
        view_click_here.isHidden = true
    }
    
}

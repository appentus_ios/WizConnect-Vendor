//
//  Model_Category.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 20/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import Foundation


class Model_Sub_Category {
    
    static let shared = Model_Sub_Category()
    
    var sub_cate_id  = ""
    var sub_cate_name = ""
    var sub_cate_code = ""
    var sub_cate_code_selected = ""
    
    var category_code = ""
    var category_id   = ""
    var category_name = ""
    var category_icon = ""
    
    var str_msg = ""
    
    var arr_sub_category_list = [Model_Sub_Category]()

    func func_get_category(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_get_subcategory
        let str_param = "cate_code=\(Model_MyProfile.shared.category_code)"
        
        API_WizConnect.func_API_Call_POST(str_URL: str_FullURL, param: str_param) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                let arr_Result = dict_JSON["result"] as? [[String:Any]]
                for dict_result in arr_Result! {
                    self.arr_sub_category_list.append(self.func_set_category_Data(dict: dict_result))
                }
            }
            completionHandler(dict_JSON["status"] as! String)
        }
    }
    
    
    private func func_set_category_Data(dict:[String:Any]) -> Model_Sub_Category {
        let model = Model_Sub_Category()
        
        model.sub_cate_id = dict["sub_cate_id"] as! String
        model.sub_cate_name = dict["sub_cate_name"] as! String
        model.sub_cate_code = dict["sub_cate_code"] as! String
        
        model.category_id = dict["category_id"] as! String
        model.category_name = dict["category_name"] as! String
        model.category_icon = dict["category_icon"] as! String
        model.category_code = dict["category_code"] as! String
        
        return model
    }
    
    var sub_category_selected = ""
    var sub_category_selected_name = ""
    
    func func_update_sub_category(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_update_sub_category
        let str_param = "vendor_id=\(Model_Splash.shared.vendor_id)&sub_category=\(sub_category_selected)"
        
        API_WizConnect.func_API_Call_POST(str_URL: str_FullURL, param: str_param) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                self.str_msg = dict_JSON["message"] as! String
                self.func_update_subcate_in_login_Data()
            }
            completionHandler(dict_JSON["status"] as! String)
        }
    }
    
    func func_update_subcate_in_login_Data() {
        if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            var dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
            print(dict_LoginData)
            
            dict_LoginData["vendor_subcate_code"] = sub_category_selected
            
            let data_dict_Result = NSKeyedArchiver.archivedData(withRootObject: dict_LoginData)
            UserDefaults .standard .setValue(data_dict_Result, forKey: "login_Data")
        }
    }
    
    
}







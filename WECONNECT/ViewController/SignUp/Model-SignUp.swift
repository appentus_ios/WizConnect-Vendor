//
//  Model-SignUp.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 20/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import Foundation
import Alamofire
import UIKit



class Model_SignUp {
    static let shared = Model_SignUp()
    
    var vendor_name = ""
    var vendor_email = ""
    var vendor_country_code = ""
    var vendor_mobile = ""
    var vendor_password = ""
    var vendor_device_type = ""
    var vendor_device_token = ""
    var vendor_location = ""
    var vendor_lat = ""
    var vendor_long = ""

    var str_selected_cate_code = ""

    var str_OTP = ""

    var str_message = ""

    var img_file = UIImage()
    var img_file_1 = UIImage()
    var img_file_2 = UIImage()

    var arr_img_works = [UIImage]()
    
    func func_UploadWithImage(completionHandler:@escaping (String)->())
    {
        let str_FullURL = k_BaseURL+k_customer_register
        
        let parameters = [
            "vendor_name":"\(vendor_name)",
            "vendor_email":"\(vendor_email)",
            "vendor_country_code":"\(vendor_country_code)",
            "vendor_mobile":"\(vendor_mobile)",
            "vendor_password":"\(vendor_password)",
            "vendor_device_type":"2",
            "vendor_device_token":"\(k_FireBaseFCMToken)",
             "vendor_location":"\(vendor_location)",
            "vendor_lat":"\(vendor_lat)",
            "vendor_long":"\(vendor_long)",
            "category":str_selected_cate_code,
            "sub_category":Model_Sub_Category.shared.sub_category_selected
            ] as [String:Any]
        
        print(parameters)
        
        let url = URL (string: str_FullURL)
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = UIImageJPEGRepresentation(self.img_file, 0.2) {
                multipartFormData.append(data, withName:"file", fileName: "image.jpg", mimeType: "image/jpg")
            }
            
            if let data = UIImageJPEGRepresentation(self.img_file_1, 0.2) {
                multipartFormData.append(data, withName:"file1", fileName: "image.jpg", mimeType: "image/jpg")
            }
            
            if let data = UIImageJPEGRepresentation(self.img_file_2, 0.2) {
                multipartFormData.append(data, withName:"file2", fileName: "image.jpg", mimeType: "image/jpg")
            }
            
            var arrayImgData = [Data]()
            
            var imageData = Data()
            for i in 0 ..< self.arr_img_works.count {
                imageData = UIImageJPEGRepresentation(self.arr_img_works[i], 0.2)!
                arrayImgData.append(imageData)
            }
            
            for i in 0..<arrayImgData.count {
                multipartFormData.append(arrayImgData[i], withName: "works[\(i)]", fileName: "image\(i).jpg", mimeType: "image/jpg")
            }
            
        }, usingThreshold: UInt64.init(), to: url!, method: .post, headers: headers) { (result) in
            
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    do {
                        let json =  try JSONSerialization .jsonObject(with:response.data!
                            , options: .allowFragments)
                        print(json)
                        
                        let dict_JSON = json as! [String:Any]
                        if dict_JSON["status"] as! String == "success" {
                            let arr_Result = dict_JSON["result"] as! [[String:String]]
                            let dict_Result = arr_Result[0]
                            
                            Model_Splash.shared.vendor_id = "\(dict_Result["vendor_id"]!)"
                            Model_Splash.shared.vendor_works_id = "\(dict_Result["vendor_works_id"]!)"
                            Model_Splash.shared.works_vendor_id = "\(dict_Result["works_vendor_id"]!)"
                            
//                            let data_dict_Result = NSKeyedArchiver.archivedData(withRootObject: dict_Result)
//                            UserDefaults .standard .setValue(data_dict_Result, forKey: "login_Data")
                        } else {
                            self.str_message = dict_JSON["message"] as! String
                        }
                        completionHandler(dict_JSON["status"] as! String)
                    }
                    catch let error as NSError {
                        print("error is:-",error)
                        completionHandler("false")
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }
    
    
    func func_SendOTP(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_send_otp
        
        let params = ["vendor_country_code":"\(vendor_country_code)",
                        "vendor_email":"\(vendor_email)",
                        "vendor_mobile":"\(vendor_mobile)"]
        
        print(params)
        
        API_WizConnect.postAPI(url: str_FullURL, parameters: params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if let str_status = dict_JSON["status"] as? String {
                if str_status == "success" {
                    self.str_OTP = "\(dict_JSON["result"]!)"
                } else {
                    self.str_message = "\(dict_JSON["message"]!)"
                }
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_message = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
    }

}

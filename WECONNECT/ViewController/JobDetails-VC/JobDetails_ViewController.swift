//
//  JobDetails_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 19/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit
import SDWebImage
import ASStarRatingView


var str_selected_Booking_id = ""

class JobDetails_ViewController: UIViewController {
    @IBOutlet weak var tbl_booking_status:UITableView!

    @IBOutlet weak var view_Container:UIView!
    @IBOutlet weak var view_Container_1:UIView!
    @IBOutlet weak var hight_cancel_accepted:NSLayoutConstraint!
    @IBOutlet weak var hieght_all_view:NSLayoutConstraint!
    
    @IBOutlet weak var view_cancel:UIView!
    @IBOutlet weak var view_reschedule:UIView!
    
    @IBOutlet weak var view_Booking_pending:UIView!
    @IBOutlet weak var img_JobInProgress:UIImageView!
    @IBOutlet weak var img_JobFinished:UIImageView!
    
    @IBOutlet weak var img_user:UIImageView!
    
    @IBOutlet weak var lbl_name:UILabel!
    @IBOutlet weak var lbl_sub_cate:UILabel!
    
    @IBOutlet weak var lbl_Booking_for_time_date:UILabel!
    @IBOutlet weak var lbl_Job_fees:UILabel!
    @IBOutlet weak var lbl_address:UILabel!
    @IBOutlet weak var lbl_request_status:UILabel!
    
    var arr_booking_status = [String]()
    
    var arr_selected_booking = [Model_Request]()
    var dict_selected_booking = Model_Request()
    
    @IBOutlet weak var view_rating:UIView!
    @IBOutlet weak var txt_comment_for_rating:UITextView!
    @IBOutlet weak var star_rating:ASStarRatingView!
    @IBOutlet weak var btn_submit:UIButton!
    @IBOutlet weak var view_rating_1:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(func_get_vendor_bookings), name: NSNotification.Name.init(rawValue: "reload_req"), object: nil)
        
        img_user.layer.cornerRadius = img_user.frame.size.width/2
        img_user.clipsToBounds = true
        
        view_Container.layer.cornerRadius = 2
        view_Container.layer.shadowOpacity = 1.0
        view_Container.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        view_Container.layer.shadowRadius = 1.0
        view_Container.layer.shadowColor = UIColor .lightGray.cgColor
        
        func_shadow(view_shadow: view_Container_1)
        func_shadow(view_shadow: view_cancel)
        func_shadow(view_shadow: view_reschedule)
        
        
        txt_comment_for_rating.layer.cornerRadius = 2
        txt_comment_for_rating.layer.borderColor=UIColor.lightGray .cgColor
        txt_comment_for_rating.layer.borderWidth=1
        
        view_rating.isHidden = true
        
        btn_submit.layer.cornerRadius = btn_submit.frame.size.height/2
        btn_submit.clipsToBounds = true
        
        func_shadow_red(view_shadow: view_rating_1)
        
        txt_comment_for_rating.layer.cornerRadius = 2
        txt_comment_for_rating.layer.borderColor = color_AppDefault.cgColor
        txt_comment_for_rating.layer.borderWidth=1
        
        func_get_vendor_bookings()
        // Do any additional setup after loading the view.
    }
    
    
    
    @objc func func_reload_req() {
        
        for model in arr_selected_booking {
            if str_selected_Booking_id == model.booking_id {
                dict_selected_booking = model
                break
            }
        }
        
        Model_Job_details.shared.booking_id =  dict_selected_booking.booking_id
        Model_Job_details.shared.receiver_id = dict_selected_booking.customer_id
        
        if dict_selected_booking.booking_status == k_pending {
            lbl_request_status.text = "Accepted"
            
        } else if dict_selected_booking.booking_status == k_accepted {
            lbl_request_status.text = "In Progress"
            arr_booking_status = ["Accepted"]
            
        } else if dict_selected_booking.booking_status == k_cancel_by_vendor || dict_selected_booking.booking_status == k_cancel_by_user{
            hight_cancel_accepted.constant = 0
            view_cancel.isHidden = true
            view_reschedule.isHidden = true
            
            arr_booking_status = ["Cancelled"]
        } else if dict_selected_booking.booking_status == k_in_progress {
            lbl_request_status.text = "Complete"
            arr_booking_status = ["In Progress"]
        } else if dict_selected_booking.booking_status == k_complete {
            hight_cancel_accepted.constant = 0
            view_cancel.isHidden = true
            view_reschedule.isHidden = true
            
            arr_booking_status = ["Completed"]
        }
        
        hieght_all_view.constant = CGFloat(460+arr_booking_status.count*80)
        tbl_booking_status.reloadData()
        
         func_set_data()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_message(_ sender:UIButton) {
        Model_Chat.shared.fri_id = dict_selected_booking.customer_id
        Model_Chat.shared .fri_name = dict_selected_booking.customer_name
        Model_Chat.shared .fri_img = dict_selected_booking.customer_profile
        
        let chat_VC = storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        present(chat_VC, animated: true, completion: nil)
    }
    
    @IBAction func btn_cancel(_ sender:Any) {
        func_cancel_request_by_vendor()
    }
    
    @IBAction func btn_accept(_ sender:UIButton) {
        if lbl_request_status.text == "Accepted" {
            func_accept_request()
        } else if lbl_request_status.text == "In Progress" {
            func_request_inprogress_vendor()
        } else if lbl_request_status.text == "Complete" {
            func_request_complete_vendor()
        }
    }
    
    @IBAction func btn_back(_ sender:Any) {
        NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "reload_req"), object: nil)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_submit_rating(_ sender:Any) {
        if star_rating.rating == 0.0 {
            self.func_ShowHud_Success(with: "Select star rating")
            DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                self.func_HideHud()
            })
            return
        }
        
        Model_Job_details.shared.comment = txt_comment_for_rating.text
        Model_Job_details.shared.rating = "\(Int(star_rating.rating))"
        
        func_ShowHud()
        Model_Job_details.shared.func_do_feedback { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status == "success" {
                    self.func_ShowHud_Success(with: Model_Job_details.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                        self.func_HideHud()
                        self.view_rating.isHidden=true
                    })
                } else {
                    self.func_ShowHud_Error(with: Model_Job_details.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                        self.func_HideHud()
                    })
                }
            }
        }
        
    }
    
    func func_set_data() {
        img_user.sd_setShowActivityIndicatorView(true)
        img_user.sd_setIndicatorStyle(.gray)
        img_user.sd_setImage(with:URL (string: dict_selected_booking.customer_profile), placeholderImage:(UIImage(named:"user-gray.png")))
        
        lbl_name.text = dict_selected_booking.customer_name
        lbl_sub_cate.text = dict_selected_booking.sub_cate_name
        lbl_Booking_for_time_date.text = "\(dict_selected_booking.booking_date) \(dict_selected_booking.booking_when)"
        lbl_Job_fees.text = "₦ \(dict_selected_booking.booking_charge) per hour"
        lbl_address.text = dict_selected_booking.booking_address
    }
    
    func func_shadow_red(view_shadow:UIView) {
        view_shadow.layer.cornerRadius = 2
        view_shadow.layer.shadowOpacity = 3.0
        view_shadow.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_shadow.layer.shadowRadius = 3.0
        view_shadow.layer.shadowColor = color_AppDefault.cgColor
    }
    
    func func_shadow(view_shadow:UIView) {
        view_shadow.layer.cornerRadius = 1
        view_shadow.layer.shadowOpacity = 3.0
        view_shadow.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_shadow.layer.shadowRadius = 3.0
        view_shadow.layer.shadowColor = UIColor .lightGray.cgColor
    }
    
    func func_circle(img_round:UIImageView) {
        img_round.layer.cornerRadius = img_round.frame.size.height/2
        img_round.clipsToBounds = true
    }
    
    
    @objc func func_get_vendor_bookings() {
        func_ShowHud()
        Model_Request.shared.func_get_vendor_bookings { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status == "success" {
                    self.arr_selected_booking = Model_Request.shared.arr_upcoming_accepted_jobs+Model_Request.shared.arr_new_job_request
                } else {
                    
                }
                self.func_reload_req()
            }
        }
    }

    
}


extension JobDetails_ViewController : UITableViewDelegate,UITableViewDataSource ,UITextViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_booking_status.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Job_details_TableViewCell
        
        cell.lbl_booking_status_name.text = arr_booking_status[indexPath.row]
        
        if arr_booking_status.count-1 == indexPath.row {
            func_shadow(view_shadow: cell.view_container)
        } else {
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    func func_cancel_request_by_vendor() {
        func_ShowHud()
        Model_Job_details.shared.func_cancel_request_by_vendor { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status == "success" {
                    self.func_ShowHud_Success(with: Model_Job_details.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                        self.func_HideHud()
                        self.func_get_vendor_bookings()
                    })
                } else {
                    self.func_ShowHud_Error(with: Model_Job_details.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                        self.func_HideHud()
                    })
                }
            }
        }
    }
    
    func func_accept_request() {
        func_ShowHud()
        Model_Job_details.shared.func_accept_request { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status == "success" {
                    self.func_ShowHud_Success(with: Model_Job_details.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                        self.func_HideHud()
                        self.func_get_vendor_bookings()
                    })
                } else {
                    self.func_ShowHud_Error(with: Model_Job_details.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                        self.func_HideHud()
                    })
                }
            }
        }
    }
    
    
    func func_request_inprogress_vendor() {
        func_ShowHud()
        Model_Job_details.shared.func_request_inprogress_vendor { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status == "success" {
                    self.func_ShowHud_Success(with: Model_Job_details.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                        self.func_HideHud()
                        self.func_get_vendor_bookings()
                    })
                } else {
                    self.func_ShowHud_Error(with: Model_Job_details.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                        self.func_HideHud()
                    })
                }
            }
        }
    }
    
    func func_request_complete_vendor() {
        func_ShowHud()
        Model_Job_details.shared.func_request_complete_vendor { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status == "success" {
                    self.func_ShowHud_Success(with: Model_Job_details.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                        self.func_HideHud()
//                      self.func_get_vendor_bookings()
                        self.arr_booking_status = ["Completed"]
                        self.tbl_booking_status.reloadData()
                        self.view_rating.isHidden = false
                        
                        self.hight_cancel_accepted.constant = 0
                        self.view_cancel.isHidden = true
                        self.view_reschedule.isHidden = true
                        
                    })
                } else {
                    self.func_ShowHud_Error(with: Model_Job_details.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                        self.func_HideHud()
                    })
                }
            }
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Put your comment here" {
            textView.textColor = UIColor .black
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.textColor = UIColor .darkGray
            textView.text = "Put your comment here"
        }
    }
    

}




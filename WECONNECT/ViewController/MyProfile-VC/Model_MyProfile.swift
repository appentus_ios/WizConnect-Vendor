//
//  Model_MyProfile.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 22/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import Foundation
import Alamofire



class Model_MyProfile {
    static let shared = Model_MyProfile()
    
    var arr_Result = [Model_MyProfile]()
    
    var category_id = ""
    var category_name = ""
    var category_icon = ""
    var category_code = ""
    
    var str_message = ""
    
    
    var vendor_name = ""
    var vendor_email = ""
    var vendor_country_code = ""
    var vendor_mobile = ""
    var vendor_password = ""
    var vendor_device_type = ""
    var vendor_device_token = ""
    var vendor_location = ""
    var vendor_lat = ""
    var vendor_long = ""
    var vendor_about = ""
    
    var str_OTP = ""
    
    
    var img_file = UIImage()

    
    
    func func_get_category(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_get_category
        
        API_WizConnect.func_API_Call_GET(apiName: str_FullURL) {
            (dict_JSON) in
            print(dict_JSON)
            
            self.arr_Result.removeAll()
            if dict_JSON["status"] as? String == "success" {
                let arr_Results = dict_JSON["result"] as! [[String:Any]]
                
                for dict in arr_Results {
                    self.arr_Result.append(self.func_set_data(dict: dict))
                }
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_message = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        

    }
    
    
  private func func_set_data(dict:[String:Any]) -> Model_MyProfile {
        let model = Model_MyProfile()
    
        model.category_id = dict["category_id"] as! String
        model.category_name = dict["category_name"] as! String
        model.category_icon = dict["category_icon"] as! String
        model.category_code = dict["category_code"] as! String
    
        return model
    }

    func func_update_category(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_update_category
        let str_param = "vendor_id=\(Model_Splash.shared.vendor_id)&category=\(category_code)"
        
        API_WizConnect.func_API_Call_POST(str_URL: str_FullURL, param: str_param) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                self.str_message = dict_JSON["message"] as! String
                
                if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
                    var dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
                    dict_LoginData["vendor_cate_code"] = "\(self.category_code)"
                    
                    let data_dict_Result = NSKeyedArchiver.archivedData(withRootObject: dict_LoginData)
                    UserDefaults .standard .setValue(data_dict_Result, forKey: "login_Data")
                }
            }
            completionHandler(dict_JSON["status"] as! String)
        }
    }
    
    
    func func_Update_Profile(completionHandler:@escaping (String)->())
    {
        let str_FullURL = k_BaseURL+k_update_profile_vendor
        
        let parameters = [
            "vendor_id":Model_Splash.shared.vendor_id,
            "vendor_cost":"",
            "vendor_name":"\(vendor_name)",
            "vendor_email":"\(vendor_email)",
            "vendor_country_code":"\(vendor_country_code)",
            "vendor_mobile":"\(vendor_mobile)",
            "vendor_device_type":"2",
            "vendor_device_token":"\(k_FireBaseFCMToken)",
            "vendor_location":"\(vendor_location)",
            "vendor_lat":"\(vendor_lat)",
            "vendor_long":"\(vendor_long)",
            "vendor_about":"\(vendor_about)"
            ] as [String:Any]
        
        print(parameters)
        
        let url = URL (string: str_FullURL)
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = UIImageJPEGRepresentation(self.img_file, 0.2) {
                multipartFormData.append(data, withName:"file", fileName: "image.jpg", mimeType: "image/jpg")
            }
            
        }, usingThreshold: UInt64.init(), to: url!, method: .post, headers: headers) { (result) in
            
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    do {
                        let json =  try JSONSerialization .jsonObject(with:response.data!
                            , options: .allowFragments)
                        print(json)
                        
                        let dict_JSON = json as! [String:Any]
                        if dict_JSON["status"] as! String == "success" {
                            let arr_Result = dict_JSON["result"] as! [[String:String]]
                            let dict_Result = arr_Result[0]
                            
//                            Model_Splash.shared.vendor_id = "\(dict_Result["vendor_id"]!)"
                            Model_Splash.shared.vendor_works_id = "\(dict_Result["vendor_works_id"]!)"
                            Model_Splash.shared.works_vendor_id = "\(dict_Result["vendor_profile"]!)"
                                                        
                            if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
                                var dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
                                
                                dict_LoginData["vendor_name"] = "\(dict_Result["vendor_name"]!)"
                                dict_LoginData["vendor_email"] = "\(dict_Result["vendor_email"]!)"
                                dict_LoginData["vendor_country_code"] = "\(dict_Result["vendor_country_code"]!)"

                                dict_LoginData["vendor_mobile"] = "\(dict_Result["vendor_mobile"]!)"
                                dict_LoginData["vendor_about"] = "\(dict_Result["vendor_about"]!)"
                                dict_LoginData["vendor_location"] = "\(dict_Result["vendor_location"]!)"
                                dict_LoginData["vendor_cate_code"] = "\(dict_Result["vendor_cate_code"]!)"
                                dict_LoginData["vendor_subcate_code"] = "\(dict_Result["vendor_subcate_code"]!)"
                                
                                dict_LoginData["vendor_lat"] = "\(dict_Result["vendor_lat"]!)"
                                dict_LoginData["vendor_long"] = "\(dict_Result["vendor_long"]!)"
                                
                                dict_LoginData["vendor_profile"] = "\(dict_Result["vendor_profile"]!)"
                                
                                let data_dict_Result = NSKeyedArchiver.archivedData(withRootObject: dict_LoginData)
                                UserDefaults .standard .setValue(data_dict_Result, forKey: "login_Data")
                            }

                            
                        }
                        completionHandler(dict_JSON["status"] as! String)
                    }
                    catch let error as NSError {
                        print("error is:-",error)
                        completionHandler("false")
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }
    
    
}




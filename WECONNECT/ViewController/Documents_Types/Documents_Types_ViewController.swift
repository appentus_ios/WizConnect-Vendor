//
//  Documents_Types_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 07/01/19.
//  Copyright © 2019 Raja Vikram singh. All rights reserved.
//

import UIKit

class Documents_Types_ViewController: UIViewController {

    @IBOutlet weak var btn_uploaded_Documents:UIButton!
    @IBOutlet weak var btn_work_station_photos:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        func_rounded_corner(button: btn_uploaded_Documents)
        func_rounded_corner(button: btn_work_station_photos)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func func_rounded_corner(button:UIButton) {
        button.layer.cornerRadius = 3
        button.clipsToBounds = true
    }
    
    @IBAction func btn_back(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }

}

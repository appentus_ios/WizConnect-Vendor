//
//  Model_Plan_Packages.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 24/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import Foundation
import Alamofire

class Model_Plan_Packages {
    static let shared = Model_Plan_Packages()
    
    var arr_get_all_plans = [Model_Plan_Packages]()
    
    var plan_id = ""
    var plan_name = ""
    var plan_limit = ""
    var plan_amount = ""
    var plan_code = ""
    
    var str_message = ""
    var remaining_days = ""
    
    func func_get_all_plan(completionHandler:@escaping (String)->()) {
        let str_FullURL = k_BaseURL+k_get_all_plan
        let params = ["vendor_id":Model_Splash.shared.vendor_id]
        
        API_WizConnect.postAPI(url: str_FullURL, parameters:params) {
            (dict_JSON) in
            print(dict_JSON)
            
            if dict_JSON["status"] as? String == "success" {
                self.remaining_days = "\(dict_JSON["remaining days"] ?? "0")"
                self.arr_get_all_plans.removeAll()
                let arr_Results = dict_JSON["result"] as! [[String:Any]]
                
                for dict in arr_Results {
                    self.arr_get_all_plans.append(self.func_set_data(dict: dict))
                }
                
                completionHandler(dict_JSON["status"] as! String)
            } else {
                if let str_status = dict_JSON["status"] as? String {
                    if str_status == "failed" {
                        self.str_message = dict_JSON["message"] as! String
                        completionHandler(dict_JSON["status"] as! String)
                    } else {
                        completionHandler("false")
                    }
                } else {
                    completionHandler("false")
                }
            }
        }
        
    }
    
    
    private func func_set_data(dict:[String:Any]) -> Model_Plan_Packages {
        let model = Model_Plan_Packages()
        
        model.plan_id = dict["plan_id"] as! String
        model.plan_name = dict["plan_name"] as! String
        model.plan_limit = "For \(dict["plan_limit"] ?? "") days"
        
        print(dict)
        model.plan_amount = "\(dict["plan_amount"] ?? "") ₦"
        print(model.plan_amount)
        
        model.plan_code = dict["plan_code"] as! String
        
        return model
    }
    
    
}




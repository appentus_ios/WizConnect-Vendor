//
//  Upload_Documents_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 27/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit
import SDWebImage


class Work_station_photos_ViewController: UIViewController {
    @IBOutlet weak var coll_workstations:UICollectionView!
    
    var arr_images = [UIImage (named: "document-2.png"),UIImage (named: "document-2.png"),UIImage (named: "document-2.png"),UIImage (named: "document-2.png"),UIImage (named: "document-2.png"),UIImage (named: "document-2.png")]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        func_get_about()
    }
    
    func func_get_about()  {
        func_ShowHud()
        Model_Documents.shared.func_get_about { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                self.coll_workstations.reloadData()
            }
        }
    }
    
    @IBAction func btn_back (_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_add(_ sender:Any) {
        func_ChooseImage()
    }
    
    func func_add(img_work:UIImage) {
        
        func_ShowHud()
        Model_Documents.func_UploadWithImage(img_works:img_work) { (dict_response) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if dict_response["status"] as! String == "success" {
                    if let str_message = dict_response["message"] as? String {
                        self.func_ShowHud_Success(with: str_message)
                    } else {
                        self.func_ShowHud_Error(with:"Try again")
                    }
                } else {
                    if let str_message = dict_response["message"] as? String {
                        self.func_ShowHud_Error(with: str_message)
                    } else {
                        self.func_ShowHud_Error(with:"Try again")
                    }
                    
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now()+1.5, execute: {
                    self.func_HideHud()
                    self.func_get_about()
                })
            }
            
        }
    }
}



//  MARK:- UICollectionView methods
extension Work_station_photos_ViewController:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize (width: collectionView.frame.size.width/2-10, height: 90)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Model_Documents.shared.arr_all_documents.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let coll_Car = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! Get_Upload_Documents_CollectionViewCell
        
        let model = Model_Documents.shared.arr_all_documents[indexPath.row]
        
        coll_Car.img_workstations.sd_setShowActivityIndicatorView(true)
        coll_Car.img_workstations.sd_setIndicatorStyle(.gray)
        coll_Car.img_workstations.sd_setImage(with:URL (string: model.vendor_works_image), placeholderImage:(UIImage(named:"document-2.png")))
        
        return coll_Car
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = Model_Documents.shared.arr_all_documents[indexPath.row]
        
        Model_Documents.shared.vendor_works_id = model.vendor_works_id
        Model_Documents.shared.works_vendor_id = model.works_vendor_id
        Model_Documents.shared.vendor_works_image = model.vendor_works_image
        
        let watch_doc = storyboard?.instantiateViewController(withIdentifier: "Watch_Work_Station_Photos_ViewController") as! Watch_Work_Station_Photos_ViewController
        present(watch_doc, animated: true, completion: nil)
    }
    
    
}




extension Work_station_photos_ViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func func_ChooseImage() {
        
        let alert = UIAlertController(title: "", message: "Please select", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            DispatchQueue.main.async {
                self.func_OpenCamera()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Photos", style: .default , handler:{ (UIAlertAction)in
            DispatchQueue.main.async {
                self.func_OpenGallary()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler:{ (UIAlertAction)in
            print("User click Delete button")
        }))
        
        alert.view.tintColor = UIColor .black
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    func func_OpenCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.delegate=self
            
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera in simulator", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func func_OpenGallary()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate=self
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            func_add(img_work: pickedImage)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
}









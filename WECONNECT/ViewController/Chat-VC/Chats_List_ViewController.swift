//
//  Chats_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 17/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit
import SDWebImage


class Chats_List_ViewController: UIViewController {
    @IBOutlet weak var view_container:UIView!
    @IBOutlet weak var tbl_chat:UITableView!
    @IBOutlet weak var lbl_no_chat:UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view_container.layer.cornerRadius = 2
        view_container.layer.shadowOpacity = 1.0
        view_container.layer.shadowOffset = CGSize(width: 0.0, height: 1.5)
        view_container.layer.shadowRadius = 1.0
        view_container.layer.shadowColor = UIColor .lightGray.cgColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        func_ShowHud()
        Model_Chat_List.shared.func_get_chat_user_list { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if Model_Chat_List.shared.arr_chat_list.count > 0 {
                    self.tbl_chat.isHidden = false
                    self.lbl_no_chat.isHidden = true
                } else {
                    self.tbl_chat.isHidden = true
                    self.lbl_no_chat.isHidden = false
                }
                
                self.tbl_chat.reloadData()
            }
        }
    }
    
}




extension Chats_List_ViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Model_Chat_List.shared.arr_chat_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell-1", for: indexPath) as! Chats_List__TableViewCell
        let model = Model_Chat_List.shared.arr_chat_list[indexPath.row]
        
        cell.img_user.sd_setShowActivityIndicatorView(true)
        cell.img_user.sd_setIndicatorStyle(.gray)
        cell.img_user.sd_setImage(with:URL (string:model.customer_profile), placeholderImage:(UIImage(named:"user-gray.png")))
        cell.lbl_name.text = model .customer_name
        cell.lbl_msg.text = model .msg
        cell.lbl_time.text = model .time
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let model = Model_Chat_List.shared.arr_chat_list[indexPath.row]
        
        Model_Chat.shared.fri_id = model.fri_id
        Model_Chat.shared .fri_name = model.customer_name
        Model_Chat.shared .fri_img = model.customer_profile

        let chat_VC = storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        present(chat_VC, animated: true, completion: nil)
    }
    
}


//
//  Model_Documents_Update.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 07/01/19.
//  Copyright © 2019 Raja Vikram singh. All rights reserved.
//

import UIKit
import Foundation
import Alamofire


class Model_Documents_Update {
    static let shared=Model_Documents_Update()
    
    var img_file = UIImage()
    var doc_key = ""
    var str_message = ""
    
    func func_update_doc(completionHandler:@escaping (String)->())
    {
        let str_FullURL = k_BaseURL+k_update_doc
        
        let parameters = [
            "vendor_id":Model_Splash.shared.vendor_id,
            "doc_key":doc_key
            ] as [String:Any]
        
        print(parameters)
        
        let url = URL (string: str_FullURL)
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = UIImageJPEGRepresentation(self.img_file, 0.2) {
                multipartFormData.append(data, withName:"file", fileName: "image.jpg", mimeType: "image/jpg")
            }
            
        }, usingThreshold: UInt64.init(), to: url!, method: .post, headers: headers) { (result) in
            
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    do {
                        let json =  try JSONSerialization .jsonObject(with:response.data!
                            , options: .allowFragments)
                        print(json)
                        
                        let dict_JSON = json as! [String:Any]
                        if dict_JSON["status"] as! String == "success" {
                            self.str_message = dict_JSON["message"] as! String
                            
                            if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
                                var dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
                                print(dict_LoginData)
                                
                                dict_LoginData[self.doc_key] = dict_JSON["result"] as! String
                                
                                let data_dict_Result = NSKeyedArchiver.archivedData(withRootObject: dict_LoginData)
                                UserDefaults .standard .setValue(data_dict_Result, forKey: "login_Data")
                            }
                            
                        }
                        completionHandler(dict_JSON["status"] as! String)
                    }
                    catch let error as NSError {
                        print("error is:-",error)
                        completionHandler("false")
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }
    

}




//
//  Watch_Documents_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 07/01/19.
//  Copyright © 2019 Raja Vikram singh. All rights reserved.
//

import UIKit
import SDWebImage



class Watch_Work_Station_Photos_ViewController: UIViewController {
    @IBOutlet weak var img_doc:UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      img_doc.sd_setShowActivityIndicatorView(true)
      img_doc.sd_setIndicatorStyle(.gray)
      img_doc.sd_setImage(with:URL (string: Model_Documents.shared.vendor_works_image), placeholderImage:(UIImage(named:"document-2.png")))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_back(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_delete(_ sender:Any) {
        let alert_VC = UIAlertController (title: "Delete Doc", message: "Are your sure?", preferredStyle: .alert)
        
        let yes_alert = UIAlertAction (title: "Yes", style: .default) { (status) in
           self.func_validation()
        }
        
        let no_alert = UIAlertAction (title: "No", style: .cancel) { (status) in
            
        }
        
        alert_VC.addAction(yes_alert)
        alert_VC.addAction(no_alert)
        alert_VC.view.tintColor = UIColor .black
        present(alert_VC, animated: true, completion: nil)
    }
    
    func func_validation() {
        if Model_Documents.shared.arr_all_documents.count < 3 {
            let alert_VC = UIAlertController (title: "Can't delete", message: "Work station photos are not greater than 2", preferredStyle: .alert)
            
            let yes_alert = UIAlertAction (title: "OK", style: .default) { (status) in
                
            }
            alert_VC.addAction(yes_alert)
            alert_VC.view.tintColor = UIColor .black
            present(alert_VC, animated: true, completion: nil)
        } else {
            self.func_delete()
        }
    }
    
    func func_delete() {
        Model_Watch_Documents.shared.doc_id = Model_Documents.shared.vendor_works_id
        
        func_ShowHud()
        Model_Watch_Documents.shared.func_delete_work_photo { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status == "success" {
                    self.func_ShowHud_Success(with: Model_Watch_Documents.shared.str_message)
                } else {
                    self.func_ShowHud_Error(with: Model_Watch_Documents.shared.str_message)
                }
                DispatchQueue.main.asyncAfter(deadline: .now()+1.5, execute: {
                    self.func_HideHud()
                    self.dismiss(animated: true, completion: nil)
                })
            }
        }

    }
    
}





//
//  Account_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 17/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit

class Account_ViewController: UIViewController {

    var arr_menu_title = ["","Your Plan","About Us","FAQs and Terms ","Contact Us","Privacy Policy","",]
    var arr_menu_images = ["","plan.png","about-us.png","terms&conditions.png","contact-us.png","privacy-policy.png","",]
    
    @IBOutlet weak var tbl_account:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        is_from_sign_up = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(update_profile), name:Notification.Name(rawValue: "update_profile"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(func_is_verified), name:Notification.Name(rawValue: "is_verified"), object: nil)
    }
    
    @objc func func_is_verified() {
        tbl_account.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        update_profile()
    }

    @objc func update_profile() {
        
        func_ShowHud()
        Model_MyProfile.shared.func_get_category { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                if status == "success" {
                    self.tbl_account.reloadData()
                }
            }
        }
        
    }
    
}



extension Account_ViewController : UITableViewDelegate,UITableViewDataSource {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row  == 0 {
            return 80
        } else {
            return 55
        }

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row  == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-1", for: indexPath) as! Account_TableViewCell
            
            if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
                let dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
                print(dict_LoginData)
                
                cell.lbl_name.text = dict_LoginData["vendor_name"] as? String
                let vendor_cate_code = dict_LoginData["vendor_cate_code"] as? String
                
                cell.img_user.sd_setShowActivityIndicatorView(true)
                cell.img_user.sd_setIndicatorStyle(.gray)
                cell.img_user.sd_setImage(with:URL (string: dict_LoginData["vendor_profile"] as! String), placeholderImage:(UIImage(named:"user-gray.png")))
                
                for model in Model_MyProfile.shared.arr_Result {
                    if model.category_code == vendor_cate_code {
                        cell.lbl_cate.text = " | \(model.category_name)"
                        
                        break
                    } else {
                        cell.lbl_cate.text = ""
                    }
                }
                
                let str_vendor_status = "\(dict_LoginData["vendor_status"] ?? "0")"
                if str_vendor_status == "0" {
                    cell.img_verified.image = UIImage (named: "notverified.png")
                    cell.lbl_verified.text = "Not Verified"
                } else {
                    cell.img_verified.image = UIImage (named: "verified.png")
                    cell.lbl_verified.text = "Verified"
                }
            } else {
                
            }
            
            return cell
        }  else if indexPath.row == 6 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-3", for: indexPath)

            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-2", for: indexPath)  as! Account_Menu_TableViewCell

            cell.lbl_menu_name.text = arr_menu_title[indexPath.row]
            cell.img_user.image = UIImage (named: arr_menu_images[indexPath.row])

            return cell
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 0  {
            let plan_packages = storyboard?.instantiateViewController(withIdentifier: "MyProfile_ViewController") as! MyProfile_ViewController
            
            present(plan_packages, animated: true, completion: nil)
        } else if indexPath.row == 1  {
            let plan_packages = storyboard?.instantiateViewController(withIdentifier: "Plan_Packages_ViewController") as! Plan_Packages_ViewController
            present(plan_packages, animated: true, completion: nil)
        }  else if indexPath.row == 2 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "About_us_ViewController") as! About_us_ViewController
            present(vc, animated: true, completion: nil)
        } else if indexPath.row == 3 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "FAQ_ViewController") as! FAQ_ViewController
            present(vc, animated: true, completion: nil)
        } else if indexPath.row == 4 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "Contact_us_ViewController") as! Contact_us_ViewController
            present(vc, animated: true, completion: nil)
        } else if indexPath.row == 5 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "Privacy_Policy_ViewController") as! Privacy_Policy_ViewController
            present(vc, animated: true, completion: nil)
        }  else if indexPath.row == 6 {
            
            let alert = UIAlertController (title: "", message: "Do you want to logout ?", preferredStyle: .alert)
            let yes = UIAlertAction (title: "Yes", style: .default) { (status) in
                UserDefaults.standard.removeObject(forKey: "login_Data")

                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignIn_ViewController") as! SignIn_ViewController
                self.present(vc, animated: true, completion: nil)
            }
            
            let no = UIAlertAction (title: "No", style: .default) { (status) in
                
            }

            alert.addAction(yes)
            alert.addAction(no)
            alert.view.tintColor = UIColor .black
            present(alert, animated: true, completion: nil)
        }
    }

    
}



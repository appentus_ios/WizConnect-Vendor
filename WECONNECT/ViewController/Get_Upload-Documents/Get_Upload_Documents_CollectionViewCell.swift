//
//  Upload_Documents_CollectionViewCell.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 17/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit

class Get_Upload_Documents_CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img_workstations:UIImageView!
    
    override func awakeFromNib() {
        img_workstations.layer.cornerRadius = 3
        img_workstations.clipsToBounds = true
    }
}

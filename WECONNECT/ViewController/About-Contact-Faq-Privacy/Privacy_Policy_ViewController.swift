//
//  Privacy_Policy_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 19/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit

class Privacy_Policy_ViewController: UIViewController {
    @IBOutlet weak var web_view: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        web_view.loadRequest(NSURLRequest(url: NSURL(fileURLWithPath: Bundle.main.path(forResource: "privacy", ofType: "html")!) as URL) as URLRequest)

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btn_back (_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

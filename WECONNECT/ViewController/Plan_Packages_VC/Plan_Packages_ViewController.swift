//
//  Plan_Packages_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 18/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit

class Plan_Packages_ViewController: UIViewController {
    @IBOutlet weak var tbl_plan:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        func_ShowHud()
        Model_Plan_Packages.shared.func_get_all_plan { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status == "success" {
                    self.tbl_plan.isHidden = false
                } else {
                    self.tbl_plan.isHidden = true
                }
                
                self.tbl_plan.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_back (_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }

}






extension Plan_Packages_ViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row  == 0 {
            return 155
        } else if indexPath.row  == 1 {
            return 50
        } else {
            return 80
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Model_Plan_Packages.shared.arr_get_all_plans.count+2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row  == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-1", for: indexPath) as! Plan_Remaining_Days_ViewController

           cell.lbl_remaining_days.text = Model_Plan_Packages.shared.remaining_days
            
            return cell
        }  else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-2", for: indexPath)
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-3", for: indexPath) as! Plan_Packages_TableViewCell
            let model = Model_Plan_Packages.shared.arr_get_all_plans[indexPath.row-2]
            
            cell.lbl_plan_name.text = model.plan_name
            cell.lbl_plan_limit.text = model.plan_limit
            cell.lbl_plan_amount.text = model.plan_amount
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row > 1 {
            let model = Model_Plan_Packages.shared.arr_get_all_plans[indexPath.row-2]
            
            Model_Plan_Packages.shared.plan_code = model.plan_code
            Model_Plan_Packages.shared.plan_name = model.plan_name
            Model_Plan_Packages.shared.plan_amount = model.plan_amount
        }
        
        let plan_packages = storyboard?.instantiateViewController(withIdentifier: "Purchase_Plan_ViewController") as! Purchase_Plan_ViewController
        present(plan_packages, animated: true, completion: nil)
    }
    
}



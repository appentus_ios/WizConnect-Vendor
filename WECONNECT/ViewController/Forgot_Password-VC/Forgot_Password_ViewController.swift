//
//  Forgot_Password_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 20/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit
import SVProgressHUD


class Forgot_Password_ViewController: UIViewController {

    @IBOutlet weak var btn_continue:UIButton!
    @IBOutlet weak var txt_email:UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        btn_continue.layer.cornerRadius = btn_continue.frame.size.height/2
        btn_continue.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_continue(_ sender:Any) {
        self.view.endEditing(true)
        
        let is_Email = func_IsValidEmail(testStr: txt_email.text!)
        
        if txt_email.text!.isEmpty {
            SVProgressHUD.showError(withStatus: "Enter your email")
            return
        } else if !is_Email {
            SVProgressHUD.showError(withStatus: "Enter valid email")
            return
        }
        
        func_ShowHud()
        
        func_forgot_password { (status) in
            
        }
        
    }
    
    @IBAction func btn_back(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func func_forgot_password(completionHandler:@escaping (String)->()) {
        let str_FullURL = "\(k_BaseURL)"+k_forget_password_customer
        
        let param = ["vendor_email":"\(txt_email.text!)"]
        print(param)
        
        API_WizConnect.postAPI(url: str_FullURL, parameters: param) {
            (dict_JSON) in
            
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if dict_JSON["status"] as? String == "success" {
                    
                    self.func_ShowHud_Success(with: dict_JSON["message"] as! String)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                    })
                    
                    completionHandler(dict_JSON["status"] as! String)
                } else {
                    if let str_status = dict_JSON["status"] as? String {
                        if str_status == "failed" {
                            self.func_ShowHud_Success(with: dict_JSON["message"] as! String)
                            DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                                self.func_HideHud()
                            })
                            
                            completionHandler(dict_JSON["status"] as! String)
                        } else {
                            completionHandler("false")
                        }
                    } else {
                        completionHandler("false")
                    }
                }
                
            }
        }
        
        
//        API_WizConnect.func
        
//        API_WizConnect.func_API_Call_POST(str_URL: str_FullURL, param: str_Params) {
//            (dict_JSON) in
//
//            DispatchQueue.main.async {
//                self.func_HideHud()
//
//                if dict_JSON["status"] as? String == "success" {
//
//                    self.func_ShowHud_Success(with: dict_JSON["message"] as! String)
//                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
//                        self.func_HideHud()
//                    })
//
//                    completionHandler(dict_JSON["status"] as! String)
//                } else {
//                    if let str_status = dict_JSON["status"] as? String {
//                        if str_status == "failed" {
//                            self.func_ShowHud_Success(with: dict_JSON["message"] as! String)
//                            DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
//                                self.func_HideHud()
//                            })
//
//                            completionHandler(dict_JSON["status"] as! String)
//                        } else {
//                            completionHandler("false")
//                        }
//                    } else {
//                        completionHandler("false")
//                    }
//                }
//
//            }
//        }
    }
    
    
}

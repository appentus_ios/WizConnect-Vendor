//
//  Notificaions_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 17/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit
import SDWebImage


class Notificaions_ViewController: UIViewController {
    @IBOutlet weak var view_container:UIView!
    @IBOutlet weak var tbl_notifications:UITableView!
    
    @IBOutlet weak var txt_search:UITextField!
    @IBOutlet weak var btn_search:UIButton!
    
    var arr_searched = [Model_Notification]()
    var arr_for_search = [Model_Notification]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txt_search.layer.cornerRadius = 3
        txt_search.clipsToBounds = true
        
        view_container.layer.cornerRadius = 2
        view_container.layer.shadowOpacity = 1.0
        view_container.layer.shadowOffset = CGSize(width: 0.0, height: 1.5)
        view_container.layer.shadowRadius = 1.0
        view_container.layer.shadowColor = UIColor .lightGray.cgColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        txt_search.isHidden = true
        btn_search.isSelected = false
        txt_search.text = ""
        
        func_ShowHud()
        Model_Notification.shared.func_get_notification_list { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status == "success" {
                    if Model_Notification.shared.arr_get_all_notifications.count == 0 {
                        self.tbl_notifications.isHidden = true
                    } else {
                        self.tbl_notifications.isHidden = false
                    }
                } else {
                    self.tbl_notifications.isHidden = true
                }
                self.arr_for_search = Model_Notification.shared.arr_get_all_notifications
                self.tbl_notifications.reloadData()
            }
        }
    }

    
    
    @IBAction func btn_Search(_ sender: UIButton) {
        btn_search.isSelected = !(btn_search.isSelected)
        
        if txt_search.isHidden {
            txt_search.isHidden = false
        } else {
            txt_search.isHidden = true
            txt_search.text = ""
            Model_Notification.shared.arr_get_all_notifications = arr_for_search
            self.view.endEditing(true)
            tbl_notifications.reloadData()
        }
        
    }
    
    
    
    @IBAction func txt_Search(_ sender: UITextField) {
        arr_searched = [Model_Notification]()
        
        for i in 0..<arr_for_search.count {
            let model = arr_for_search[i]
            let target = model.title
            if ((target as NSString?)?.range(of:txt_search.text!, options: .caseInsensitive))?.location == NSNotFound
            { } else {
                arr_searched.append(model)
            }
        }
        
        if (txt_search.text! == "") {
            arr_searched = arr_for_search
        }
        
        Model_Notification.shared.arr_get_all_notifications = arr_searched
        tbl_notifications.reloadData()
    }
    

    
}




extension Notificaions_ViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Model_Notification.shared.arr_get_all_notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell-1", for: indexPath) as! Notifications_TableViewCell
        
        let model = Model_Notification.shared.arr_get_all_notifications[indexPath.row]
        
        cell.img_user.sd_setShowActivityIndicatorView(true)
        cell.img_user.sd_setIndicatorStyle(.gray)
        cell.img_user.sd_setImage(with:URL (string: model.customer_profile), placeholderImage:(UIImage(named:"user-gray.png")))
        
        cell.lbl_name_title.text = model.title
        cell.lbl_time_date.text = "\(model.date)"//\(model.time)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}


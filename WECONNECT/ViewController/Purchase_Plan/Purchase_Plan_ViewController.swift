//
//  Purchase_Plan_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 18/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit

class Purchase_Plan_ViewController: UIViewController {
    
    let arr_card_title = ["Pay through bank","Visa or mastercard"]
    let arr_card_sub_title = ["Pay using your preferred bank","Paystack"]
    let arr_card_icon = ["credit-card.png","credit-card.png"]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_back (_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }

}



extension Purchase_Plan_ViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row  == 0 {
            return 130
        } else if indexPath.row  == 1 {
            return 50
        } else {
            return 100
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_card_title.count+2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row  == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-1", for: indexPath)
            
            let lbl_plan_name = cell.viewWithTag(1) as! UILabel
            let lbl_plan_amount = cell.viewWithTag(2) as! UILabel
            
            lbl_plan_name.text = Model_Plan_Packages.shared.plan_name
            lbl_plan_amount.text = Model_Plan_Packages.shared.plan_amount
            
            return cell
        }  else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-2", for: indexPath)
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-3", for: indexPath)
            
            let lbl_card_icon = cell.viewWithTag(1) as? UIImageView
            let lbl_card_title = cell.viewWithTag(2) as? UILabel
            let lbl_card_sub_title = cell.viewWithTag(3) as? UILabel
            
            lbl_card_title?.text = arr_card_title[indexPath.row-2]
            lbl_card_sub_title?.text = arr_card_sub_title[indexPath.row-2]
            lbl_card_icon?.image = UIImage (named: arr_card_icon[indexPath.row-2])
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 2 {
            let payStack = storyboard?.instantiateViewController(withIdentifier: "Bank_Details_ViewController") as! Bank_Details_ViewController
            present(payStack, animated: true, completion: nil)
        } else {
            let pay_stack_VC = storyboard?.instantiateViewController(withIdentifier: "Pay_Stack_1_ViewController") as! Pay_Stack_1_ViewController
            present(pay_stack_VC, animated: true, completion: nil)
        }
    }
    
    
    
}





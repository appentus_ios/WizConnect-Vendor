//
//  Reviews_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 19/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit
import ASStarRatingView



class Reviews_ViewController: UIViewController {
    
    @IBOutlet weak var btn_readAllReviews :UIButton!
    
    @IBOutlet weak var progress_1 :UIProgressView!
    @IBOutlet weak var progress_2 :UIProgressView!
    @IBOutlet weak var progress_3 :UIProgressView!
    @IBOutlet weak var progress_4 :UIProgressView!
    @IBOutlet weak var progress_5 :UIProgressView!
    
    @IBOutlet weak var lbl_1:UILabel!
    @IBOutlet weak var lbl_2:UILabel!
    @IBOutlet weak var lbl_3:UILabel!
    @IBOutlet weak var lbl_4:UILabel!
    @IBOutlet weak var lbl_5:UILabel!
    
    @IBOutlet weak var lbl_Current_Rating:UILabel!
    @IBOutlet weak var lbl_People_Reviewed:UILabel!
    @IBOutlet weak var lbl_Task_completed:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btn_readAllReviews.layer.cornerRadius = btn_readAllReviews.frame.size.height/2
        btn_readAllReviews.clipsToBounds = true
        
        func_roundCorner_Progress(progress: progress_1)
        func_roundCorner_Progress(progress: progress_2)
        func_roundCorner_Progress(progress: progress_3)
        func_roundCorner_Progress(progress: progress_4)
        func_roundCorner_Progress(progress: progress_5)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        func_get_feedback()
    }
    
    func func_get_feedback()  {
        func_ShowHud()
        Model_Reviews.shared.func_get_feedback { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                if status == "success" {
                    self.lbl_People_Reviewed.text = "\(Model_Reviews.shared.arr_feedbacks.count)"
                    self.lbl_Task_completed.text = Model_Reviews.shared.order_count
                    
                    var all_rating = 0.0
                    for model in Model_Reviews.shared.arr_feedbacks {
                        all_rating = all_rating+Double(model.rating)!
                    }
                    
                    let avg_rating = all_rating/Double(Model_Reviews.shared.arr_feedbacks.count)
                    self.lbl_Current_Rating.text = String (format: "%.1f", avg_rating)
                    
                    DispatchQueue.main.async {
                        self.func_set_rating_particular()
                    }
                } else {
                    self.lbl_People_Reviewed.text = "0"
                    self.lbl_Current_Rating.text = "0"
                }
            }
        }

    }
    
    func func_set_rating_particular() {
        var rating_1 = 0
        var rating_2 = 0
        var rating_3 = 0
        var rating_4 = 0
        var rating_5 = 0
        
        for model in Model_Reviews.shared.arr_feedbacks {
            if model.rating == "1" {
                rating_1 = rating_1+1
            } else if model.rating == "2" {
                rating_2 = rating_1+1
            } else if model.rating == "3" {
                rating_3 = rating_1+1
            } else if model.rating == "4" {
                rating_4 = rating_1+1
            } else if model.rating == "5" {
                rating_5 = rating_1+1
            }
        }
        
        progress_1.progress = Float(Double(rating_1)/Double(Model_Reviews.shared.arr_feedbacks.count))
        progress_2.progress = Float(Double(rating_2)/Double(Model_Reviews.shared.arr_feedbacks.count))
        progress_3.progress = Float(Double(rating_3)/Double(Model_Reviews.shared.arr_feedbacks.count))
        progress_4.progress = Float(Double(rating_4)/Double(Model_Reviews.shared.arr_feedbacks.count))
        progress_5.progress = Float(Double(rating_5)/Double(Model_Reviews.shared.arr_feedbacks.count))
        
        lbl_1.text = "\(rating_1)"
        lbl_2.text = "\(rating_2)"
        lbl_3.text = "\(rating_3)"
        lbl_4.text = "\(rating_4)"
        lbl_5.text = "\(rating_5)"
    }
    
    func func_roundCorner_Progress (progress:UIProgressView) {
        progress.layer.cornerRadius = 6
        progress.clipsToBounds = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btn_ReadYourReviews(_ sender:Any) {
        let your_Reviews_VC = storyboard?.instantiateViewController(withIdentifier: "Your_Reviews_ViewController") as! Your_Reviews_ViewController
        present(your_Reviews_VC, animated: true, completion: nil)
    }
    
}

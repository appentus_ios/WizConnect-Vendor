//
//  Your_Reviews_TableViewCell.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 24/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//

import UIKit
import ASStarRatingView

class Your_Reviews_TableViewCell: UITableViewCell {
    @IBOutlet weak var view_container:UIView!

    @IBOutlet weak var star:ASStarRatingView!
    
    @IBOutlet weak var customer_name:UILabel!
    @IBOutlet weak var img_customer_profile:UIImageView!
    @IBOutlet weak var date:UILabel!
    @IBOutlet weak var comment:UITextView!
    @IBOutlet weak var rating:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        view_container.layer.cornerRadius = 2
        view_container.layer.shadowOpacity = 1.0
        view_container.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_container.layer.shadowRadius = 3.0
        view_container.layer.shadowColor = UIColor .lightGray.cgColor
        
        // Configure the view for the selected state
    }

}

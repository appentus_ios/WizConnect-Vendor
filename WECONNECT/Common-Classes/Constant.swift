//
//  Constant.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 20/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.
//


import Foundation
import UIKit
import SVProgressHUD

var k_pending = "0"
var k_accepted = "1"
var k_cancel_by_vendor = "2"
var k_cancel_by_user = "3"
var k_in_progress = "4"
var k_complete = "5"

var k_pending_color = UIColor .lightGray
var k_accepted_color = UIColor .green
var k_cancel_by_vendor_color = UIColor .red
var k_cancel_by_user_color = UIColor .red
var k_in_progress_color = UIColor .green
var k_complete_color = UIColor .yellow


//  MARK:- FireBase FCM Token

// MARK:- Base URL
//let k_BaseURL = "http://appentus.me/vconn/api/api/"
//let k_BaseURL_chat = "http://appentus.me/vconn/api/chat/"

let k_BaseURL = "http://www.wizconnect.net/admin/api/api/"
let k_BaseURL_chat = "http://www.wizconnect.net/admin/api/chat/"

//MARK:- Rest URL
let k_send_otp = "send_otp_vendor"
let k_customer_register = "vendor_register"

let k_customer_login = "vendor_login"
let k_forget_password_customer = "forget_password_vendor"

let K_get_category = "get_category"
let k_get_subcategory = "get_subcategory"
let k_vendor_list = "vendor_list"
let k_book_vendor = "book_vendor"
let k_get_category = "get_category"

let k_update_category = "update_category"
let k_update_sub_category = "update_sub_category"
let k_update_profile_vendor = "update_profile_vendor"
let k_get_feedback = "get_feedback"
let k_get_all_plan = "get_all_plan"
let k_chat_msg = "get_user_chat"
let k_insert_chat = "insert_chat"
let k_get_chat_user_list = "get_chat_user_list"
let k_get_vendor_bookings = "get_vendor_bookings"
let k_get_vendor_complete_bookings = "get_vendor_complete_bookings"
let k_cancel_request_by_vendor = "cancel_request_by_vendor"
let k_accept_request = "accept_request"
let K_request_inprogress_vendor = "request_inprogress_vendor"
let k_request_complete_vendor = "request_complete_vendor"
let k_do_feedback = "do_feedback"
let k_purchase_plan = "purchase_plan"
let k_get_about = "get_about"
let k_delete_work_photo = "delete_work_photo"
let k_add_work_photo = "add_work_photo"
let k_update_doc = "update_doc"
let k_get_notification_list = "get_notification_list"



// MARK:- App-Color
extension UIColor {
    class func k_GreenColor() -> UIColor {
        return UIColor (red: 113.0/255.0, green: 200.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    }
}

extension UIViewController {
    
    func func_showHudWithStatus(with str_Error:String)  {
        SVProgressHUD.show(withStatus: str_Error)
    }
    
    func func_ShowHud()  {
        DispatchQueue.main.async {
//            SVProgressHUD.setBackgroundColor(UIColor .lightGray)
            
            if Reachability.isConnectedToNetwork() {
                SVProgressHUD.show()
                self.view.isUserInteractionEnabled = false
            } else {
                SVProgressHUD.showError(withStatus: "Internet Connection not Available!")
            }
            
            self.view.isUserInteractionEnabled = false
        }
    }
    
    func func_HideHud()  {
        DispatchQueue.main.async {
            SVProgressHUD .dismiss()
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func func_ShowHud_Success(with str_Error:String) {
        DispatchQueue.main.async {
//            SVProgressHUD.setBackgroundColor(UIColor .lightGray)
            SVProgressHUD.showSuccess(withStatus: str_Error)
            self.view .isUserInteractionEnabled = false
        }
    }
    
    func func_ShowHud_Error(with str_Error:String) {
        DispatchQueue.main.async {
//            SVProgressHUD.setBackgroundColor(UIColor .lightGray)
            SVProgressHUD.showError(withStatus: str_Error)
            self.view .isUserInteractionEnabled = false
        }
    }
    
    func func_IsValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}



extension UIColor {
    class func func_GreenColor() -> UIColor {
        return UIColor (red: 115.0/255.0, green: 201.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    }
}


extension UIViewController {
    
    func func_Current_Date_Time() -> (time: String, date: String) {
        let myDate = Date()
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd.MM.YYYY"
        let str_CurrentDate = dateFormat.string(from: myDate)
        
        dateFormat.dateFormat = "hh.mm.ss a"
        let  str_CurrentTime = dateFormat.string(from: myDate)
        
        return (str_CurrentTime , str_CurrentDate)
    }
    
    
}






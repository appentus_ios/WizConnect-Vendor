//
//  Documents_Update_ViewController.swift
//  WECONNECT
//
//  Created by iOS-Appentus on 07/01/19.
//  Copyright © 2019 Raja Vikram singh. All rights reserved.
//

import UIKit
import SDWebImage


class Documents_Update_ViewController: UIViewController {

    @IBOutlet weak var img_doc_1:UIImageView!
    @IBOutlet weak var img_doc_2:UIImageView!
    @IBOutlet weak var img_doc_3:UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        func_set_doc()
    }
    
    func func_set_doc(){
        if let data_LoginData = UserDefaults.standard.object(forKey: "login_Data") as? Data {
            let dict_LoginData = NSKeyedUnarchiver.unarchiveObject(with: data_LoginData) as! [String: Any]
            print(dict_LoginData)
            
            img_doc_1.sd_setShowActivityIndicatorView(true)
            img_doc_1.sd_setIndicatorStyle(.gray)
            img_doc_1.sd_setImage(with:URL (string: dict_LoginData["vendor_doc1"] as! String), placeholderImage:(UIImage(named:"document-2.png")))
            
            img_doc_2.sd_setShowActivityIndicatorView(true)
            img_doc_2.sd_setIndicatorStyle(.gray)
            img_doc_2.sd_setImage(with:URL (string: dict_LoginData["vendor_doc2"] as! String), placeholderImage:(UIImage(named:"document-2.png")))
            
            img_doc_3.sd_setShowActivityIndicatorView(true)
            img_doc_3.sd_setIndicatorStyle(.gray)
            img_doc_3.sd_setImage(with:URL (string: dict_LoginData["vendor_doc3"] as! String), placeholderImage:(UIImage(named:"document-2.png")))
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_upload_doc_1(_ sender:UIButton) {
        Model_Documents_Update.shared.doc_key = "vendor_doc1"
        
        func_ChooseImage()
    }
    
    @IBAction func btn_upload_doc_2(_ sender:UIButton) {
        Model_Documents_Update.shared.doc_key = "vendor_doc2"
        func_ChooseImage()
    }
    
    @IBAction func btn_upload_doc_3(_ sender:UIButton) {
        Model_Documents_Update.shared.doc_key = "vendor_doc3"
        func_ChooseImage()
    }
    
    @IBAction func btn_back(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
}



extension Documents_Update_ViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func func_ChooseImage() {
        
        let alert = UIAlertController(title: "", message: "Select an image for update", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            DispatchQueue.main.async {
                self.func_OpenCamera()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Photos", style: .default , handler:{ (UIAlertAction)in
            DispatchQueue.main.async {
                self.func_OpenGallary()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler:{ (UIAlertAction)in
            print("User click Delete button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    func func_OpenCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.delegate=self
            
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera in simulator", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func func_OpenGallary()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate=self
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            Model_Documents_Update.shared.img_file = pickedImage
        }
        
        dismiss(animated: true) {
            DispatchQueue.main.async {
                let alert = UIAlertController (title: "", message: "Do you want to update?", preferredStyle: .alert)
                let yes = UIAlertAction (title: "Yes", style: .default) { (status) in
                    self.func_upload_docs()
                }
                
                let no = UIAlertAction (title: "No", style: .default) { (status) in
                    
                }
                
                alert.addAction(yes)
                alert.addAction(no)
                alert.view.tintColor = UIColor .black
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    func func_upload_docs() {
        func_ShowHud()
        Model_Documents_Update.shared.func_update_doc { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                if status == "success" {
                    self.func_ShowHud_Success(with: Model_Documents_Update.shared.str_message)
                    self.func_set_doc()
                } else {
                    self.func_ShowHud_Error(with: Model_Documents_Update.shared.str_message)
                }
                DispatchQueue.main.asyncAfter(deadline: .now()+1.5, execute: {
                    self.func_HideHud()
                })
            }
        }
    }
    
}










//
//  SignUp_ViewController.swift
//  WECONNECT
//  Created by iOS-Appentus on 17/12/18.
//  Copyright © 2018 Raja Vikram singh. All rights reserved.


import UIKit
import CountryPicker
import SVProgressHUD
import CoreLocation

var str_selectYourLocations = ""
var selected_Co_Ordinates = CLLocationCoordinate2DMake(0.0, 0.0)

class SignUp_ViewController: UIViewController {
    
    @IBOutlet weak var txt_name:UITextField!
    @IBOutlet weak var txt_email:UITextField!
    @IBOutlet weak var txt_mobile:UITextField!
    @IBOutlet weak var txt_password:UITextField!
    @IBOutlet weak var txt_location:UITextField!
    
    @IBOutlet weak var txt_OTP:UITextField!
    
    @IBOutlet weak var lbl_Timer:UILabel!
    
    @IBOutlet weak var picker: CountryPicker!
    
    @IBOutlet weak var btn_signUpNow:UIButton!
    @IBOutlet weak var btn_Terms_Conditions:UIButton!
    @IBOutlet weak var btn_send_OTP:UIButton!
    
    @IBOutlet weak var view_CountryPicker: UIView!
    
    @IBOutlet weak var btn_CountryCode: UIButton!
    
    var timer_count = 30
    
    var gameTimer: Timer!
    
    @IBOutlet weak var view_picker:UIView!
    @IBOutlet weak var picker_profile:UIPickerView!
    var str_selected_cate_code = ""
    
    @IBOutlet weak var tx_select_job_category:UITextField!
    @IBOutlet weak var lbl_subcategory:UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        is_from_sign_up = true
        is_remaining_days = false
        
        view_picker.isHidden = true
        view_CountryPicker.isHidden = true
        func_AddCountryPicker()
        
        func_get_category()
        
        NotificationCenter.default.addObserver(self, selector: #selector(func_Send_Location), name:Notification.Name(rawValue: "send_Location_Chat"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(subCate), name:Notification.Name(rawValue: "subCate"), object: nil)
        
        txt_OTP.layer.borderColor = UIColor .lightGray.cgColor
        txt_OTP.layer.borderWidth = 1
        
        btn_signUpNow.layer.cornerRadius = btn_signUpNow.frame.size.height/2
        btn_signUpNow.clipsToBounds = true
        
        let underlineAttribute : [NSAttributedStringKey: Any] = [
            NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue) : UIFont (name: "Lato-Regular", size:  12.0) ?? UIFont .systemFont(ofSize: 12),
            NSAttributedStringKey(rawValue: NSAttributedStringKey.underlineColor.rawValue) : UIColor.lightGray,
            NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue) : UIColor.lightGray,
            NSAttributedStringKey(rawValue: NSAttributedStringKey.underlineStyle.rawValue) :NSUnderlineStyle.styleSingle.rawValue]
        
        let underlineAttributedString = NSAttributedString(string: "Terms & Conditions", attributes: underlineAttribute)
        
        btn_Terms_Conditions.setAttributedTitle(underlineAttributedString, for: .normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_terms_conditions(_ sender: Any) {
        let terms_condtion_VC = storyboard?.instantiateViewController(withIdentifier: "FAQ_ViewController") as! FAQ_ViewController
        present(terms_condtion_VC, animated: true, completion: nil)
    }

    @objc func func_Send_Location() {
        txt_location.text = str_selectYourLocations
    }
    
    @objc func subCate() {
        lbl_subcategory.text = Model_Sub_Category.shared.sub_category_selected_name
    }
    
    @IBAction func btn_selectlocation(_ sender:Any) {
        let map_VC = storyboard?.instantiateViewController(withIdentifier: "Map_ViewController") as! Map_ViewController
        present(map_VC, animated: true, completion: nil)
        
    }
    
    @IBAction func btn_back(_ sender:Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_sendOTP(_ sender:Any) {
        if !func_Validation_OTP() {
            return
        }
        
        Model_SignUp.shared.vendor_country_code = btn_CountryCode.currentTitle!
        Model_SignUp.shared.vendor_email = txt_email.text!
        Model_SignUp.shared.vendor_mobile = txt_mobile.text!
        
        func_ShowHud()
        Model_SignUp.shared.func_SendOTP { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                if status != "success" {
                    self.func_ShowHud_Success(with: Model_SignUp.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                        self.func_HideHud()
                    })
                    
                } else if status == "success" {
                    self.gameTimer = Timer.scheduledTimer(timeInterval:1, target: self, selector: #selector(self.runTimedCode), userInfo: nil, repeats: true)
                    self.timer_count = 30
                    
                    self.func_ShowHud_Success(with: Model_SignUp.shared.str_message)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                        self.func_HideHud()
                    })
                    
                }
            }
        }
    }
    
    @objc func runTimedCode() {
        timer_count = timer_count-1
        
        lbl_Timer.text = "\(timer_count)"
        
        if timer_count == 0 {
            gameTimer.invalidate()
            btn_send_OTP.setTitle("Resend OTP", for: .normal)
        }
        
    }
    
    @IBAction func btn_SignUp(_ sender:UIButton) {
        
        if !func_Validation() {
            return
        }
        
        Model_SignUp.shared.vendor_name = txt_name.text!
        Model_SignUp.shared.vendor_email = txt_email.text!
        Model_SignUp.shared.vendor_country_code = btn_CountryCode.currentTitle!
        Model_SignUp.shared.vendor_mobile = txt_mobile.text!
        Model_SignUp.shared.vendor_password = txt_password.text!
        Model_SignUp.shared.str_selected_cate_code = str_selected_cate_code
        
        Model_SignUp.shared.vendor_location = txt_location.text!
        Model_SignUp.shared.vendor_lat = "\(selected_Co_Ordinates.latitude)"
        Model_SignUp.shared.vendor_long = "\(selected_Co_Ordinates.longitude)"
        
        let vc=self.storyboard?.instantiateViewController(withIdentifier: "Upload_Docuements_ViewController") as! Upload_Docuements_ViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btn_CancelPicker(_ sender: UIButton) {
        view_CountryPicker.isHidden = true
    }
    
    @IBAction func btn_DoneCoutrnyPicker(_ sender: UIButton) {
        view_CountryPicker.isHidden = true
    }
    
    @IBAction func btn_CountryCode(_ sender: UIButton) {
        view_CountryPicker.isHidden = false
        self.view.endEditing(true)
    }
    
}



extension SignUp_ViewController :UITextFieldDelegate ,CountryPickerDelegate {
    
    //    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    //        if textField == txt_selectYourLocations {
    //
    //            let map_VC=storyboard?.instantiateViewController(withIdentifier: "Map_ViewController") as! Map_ViewController
    //            present(map_VC, animated: true, completion: nil)
    //
    //            return false
    //        } else {
    //            return true
    //        }
    
    //    }
    
    func func_AddCountryPicker() {
        let locale = Locale.current
        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
        //        picker.displayOnlyCountriesWithCodes = ["DK", "SE", "NO", "DE"] //display only
        //        picker.exeptCountriesWithCodes = ["RU"] //exept country
        let theme = CountryViewTheme(countryCodeTextColor: .black, countryNameTextColor: .black, rowBackgroundColor: .white, showFlagsBorder: false)        //optional for UIPickerView theme changes
        picker.theme = theme //optional for UIPickerView theme changes
        picker.countryPickerDelegate = self
        picker.showPhoneNumbers = true
        picker.setCountry(code!)
    }
    
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        
        print(phoneCode)
        print(countryCode)
        
        btn_CountryCode.setTitle(phoneCode, for:.normal)
    }
    
    
    
    func func_Validation_OTP() -> Bool {
        let is_Email = func_IsValidEmail(testStr: txt_email.text!)
        
        if txt_email.text!.isEmpty {
            func_ShowHud_Error(with: "Enter Email")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+0.5) {
                self.func_HideHud()
            }
            return false
        } else if !is_Email {
            func_ShowHud_Error(with: "Enter valid Email")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+0.5) {
                self.func_HideHud()
            }
            return false
        } else if txt_mobile.text!.isEmpty {
            func_ShowHud_Error(with: "Enter Phone number")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+0.5) {
                self.func_HideHud()
            }
            return false
        } else {
            return true
        }
    }
    
    func func_Validation() -> Bool {
        let is_Email = func_IsValidEmail(testStr: txt_email.text!)
        
        if txt_name.text!.isEmpty {
            func_ShowHud_Error(with: "Enter your name")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+0.5) {
                self.func_HideHud()
            }
            return false
        } else if txt_email.text!.isEmpty {
            func_ShowHud_Error(with: "Enter Email")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+0.5) {
                self.func_HideHud()
            }
            return false
        } else if !is_Email {
            func_ShowHud_Error(with: "Enter valid Email")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+0.5) {
                self.func_HideHud()
            }
            return false
        } else if txt_mobile.text!.isEmpty {
            func_ShowHud_Error(with: "Enter Phone number")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+0.5) {
                self.func_HideHud()
            }
            return false
        } else if txt_password.text!.isEmpty {
            func_ShowHud_Error(with: "Enter Password")
            
            DispatchQueue.main .asyncAfter(deadline:.now()+0.5) {
                self.func_HideHud()
            }
            return false
        }
        
//        else if txt_OTP.text!.isEmpty {
//            func_ShowHud_Error(with: "Enter OTP")
//
//            DispatchQueue.main .asyncAfter(deadline:.now()+0.5) {
//                self.func_HideHud()
//            }
//            return false
//        } else if txt_OTP.text != Model_SignUp.shared.str_OTP {
//            func_ShowHud_Error(with: "Wrong OTP")
//
//            DispatchQueue.main .asyncAfter(deadline:.now()+0.5) {
//                self.func_HideHud()
//            }
//            return false
//        } else if txt_location.text!.isEmpty {
//            func_ShowHud_Error(with: "Select your location")
//
//            DispatchQueue.main .asyncAfter(deadline:.now()+0.5) {
//                self.func_HideHud()
//            }
//            return false
//        }
        else {
            return true
        }
    }
    
    
    
}




extension SignUp_ViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
     func func_get_category() {
        func_ShowHud()
        Model_MyProfile.shared.func_get_category { (status) in
            DispatchQueue.main.async {
                self.func_HideHud()
                if status == "success" {
                    
                }
            }
        }
        
    }
    
    @IBAction func btn_select_category (_ sender:UIButton) {
        self.view_picker.isHidden = false
        self.picker_profile.reloadAllComponents()
    }
    
    @IBAction func btn_select_services_You_Provides (_ sender:UIButton) {
        if !Model_MyProfile.shared.category_code.isEmpty {
            let category_VC = storyboard?.instantiateViewController(withIdentifier: "Category_ViewController") as! Category_ViewController
            present(category_VC, animated: true, completion: nil)
        } else {
            SVProgressHUD.showError(withStatus: "Select a category")
            
            return
        }
        
    }

    
    
    @IBAction func btn_done_picker (_ sender:UIButton) {
        view_picker.isHidden = true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Model_MyProfile.shared.arr_Result.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let model = Model_MyProfile.shared.arr_Result[row]
        
        return model.category_name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let model = Model_MyProfile.shared.arr_Result[row]
        
        Model_MyProfile.shared.category_id = model.category_id
        Model_MyProfile.shared.category_code = model.category_code
        str_selected_cate_code = Model_MyProfile.shared.category_code
        Model_MyProfile.shared.category_icon = model.category_icon
        Model_MyProfile.shared.category_name = model.category_name
        
        self.tx_select_job_category.text = model.category_name
    }

    
}



